/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springinaction.knights;

import java.io.PrintStream;


/**
 *
 * @author Gon
 */
public class SlayDragonQuest implements Quest {
    
    private String stream;
    
    public SlayDragonQuest(String stream){
        this.stream = stream;
    }
    
    public void embark(){
        System.out.println(stream);
    }
    
    
}
