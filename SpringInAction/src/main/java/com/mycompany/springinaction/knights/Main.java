/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springinaction.knights;

import com.mycompany.springinaction.knights.config.KnightConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Gon
 */
public class Main {
    
    public static void main (String []args)throws Exception{
        
        //Using ClassPathXmlApplicationContext, loads a context definition from one or more XML files
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
        "SpringAction.xml");
        BraveKnight knight = (BraveKnight) context.getBean("knight");
        knight.embarkOnQuest();
        
        //Using AnnotationConfigApplicationContext- Load a Spring application context from one or more Java-based configuration classes
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(KnightConfig.class);
        ctx.refresh();
        Knight heroKnight = ctx.getBean(Knight.class);
        heroKnight.embarkOnQuest();
        
    }
    
    
}
