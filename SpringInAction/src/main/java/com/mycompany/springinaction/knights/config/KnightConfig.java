/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springinaction.knights.config;

import com.mycompany.springinaction.knights.BraveKnight;
import com.mycompany.springinaction.knights.Knight;
import com.mycompany.springinaction.knights.Quest;
import com.mycompany.springinaction.knights.SlayDragonQuest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Gon
 */
@Configuration
public class KnightConfig {
  
    @Bean
    public Knight knight(){
        return new BraveKnight(quest());
    }
    
    @Bean
    public Quest quest(){
        return new SlayDragonQuest("this is slaydragon from annotation");
    }
    
}
