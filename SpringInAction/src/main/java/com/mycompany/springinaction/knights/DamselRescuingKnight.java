/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springinaction.knights;

/**
 *
 * @author Gon
 */
public class DamselRescuingKnight implements Knight {
        private RescueDamselQuest quest;
        
        public DamselRescuingKnight(){
            this.quest = new RescueDamselQuest();
            
        }
    
    public void embarkOnQuest(){
        quest.embark();
    }
}
