/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springinaction;

import com.mycompany.springinaction.knights.BraveKnight;
import com.mycompany.springinaction.knights.Quest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 *
 * @author Gon
 */
public class BraveKnightTest {
    
    public BraveKnightTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of embarkOnQuest method, of class BraveKnight.
     */
    @Test
    public void testEmbarkOnQuest() {
       Quest mockQuest = mock(Quest.class);
       BraveKnight knight = new BraveKnight(mockQuest);
       knight.embarkOnQuest();
       verify(mockQuest,times(1)).embark();
    }
    
}
