Application can be run in 2 mode: Test and Production:

-Back end java project, stimulate a flooring company which offer 4 types of flooring: Laminate, Carpet, Tile, Wood with different material and labor costs.

-Allow user to display all the orders for a particular day, add new order by choosing flooring type and area, remove and edit an order.

-Allow user to save work or not before exit.

-Test mode is fully function without the ability to save any work to file system.

(This is a pair project of Tai Nguyen and Geoffrey Hinck).
