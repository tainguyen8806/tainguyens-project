/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.ui;

/**
 *
 * @author apprentice
 */
public interface consoleInterface {

    public int readInteger(String p);

    public int readInteger(String p, int min, int max);

    public String readString(String p);

    public float readFloat(String p);

    public float readFloat(String p, float min, float max);

    public double readDouble(String p);

    public double readDouble(String p, double min, double max);

    public void write(String p);

}
