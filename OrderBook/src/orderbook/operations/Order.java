/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

/**
 *
 * @author apprentice
 */
public class Order implements printInterface, orderInterface {

    private String orderNum;
    private String customerName;
    private String state;
    private double taxRate;
    private Product productType;
    private double costPerSqft;
    private double laborPerSqft;
    private double area;
    private double materialCost;
    private double laborCost;
    private double totalCost;
    private double tax;

    public Order(String orderNumIn, String customerNameIn, String stateIn, double taxRateIn, Product typeIn, double areaIn) {
        orderNum = orderNumIn;
        customerName = customerNameIn;
        state = stateIn;
        taxRate = taxRateIn;
        productType = typeIn;
        //get rid of?
        costPerSqft = typeIn.getCostPerSqft();
        laborPerSqft = typeIn.getLaborPerSqft();
        area = areaIn;
        materialCost = costPerSqft * area;
        laborCost = laborPerSqft * area;
        tax = (laborCost + materialCost) * taxRate;
        totalCost = materialCost + laborCost + tax;

    }

    public Order(String customerNameIn) {
        customerName = customerNameIn;
        //FILL THIS OUT WITH EMPTY VALUES FOR MOST, GENERATED FOR SOME
    }

    @Override
    public String write() {
        return String.format("%s,%s,%s,%.2f,%s,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f", orderNum, customerName, state, taxRate,
                getProductType().getType(), area, getProductType().getCostPerSqft(), getProductType().getLaborPerSqft(),
                materialCost, laborCost, getTax(), getTotalCost());
    }

    @Override
    public String toString() {
        return getOrderNum() + "," + customerName + "," + state + "," + taxRate + ","
                + getProductType().getCostPerSqft() + ", " + area + ", " + getProductType().getType()
                + "," + getProductType().getLaborPerSqft() + "," + materialCost + "," + laborCost + tax + totalCost; // tax and total
    }

    /**
     * @return the orderNum
     */
    @Override
    public String getOrderNum() {
        return orderNum;
    }

    /**
     * @param orderNum the orderNum to set
     */
    @Override
    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * @return the customerName
     */
    @Override
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    @Override
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the state
     */
    @Override
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    @Override
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the taxRate
     */
    @Override
    public double getTaxRate() {
        return taxRate;
    }

    @Override
    public void setTaxRate(double rate) {
        this.taxRate = rate;
    }

    @Override
    public Product getProductType() {
        return this.productType;
    }
    /*
     @Override
     public Product setProductType(int i) {
     switch (i) {
     case 1:
     this.productType = new Product("Carpet");
     break;
     case 2:
     this.productType = new Product("Laminate");
     break;
     case 3:
     this.productType = new Product("Tile");
     break;
     case 4:
     this.productType = new Product("Wood");
     break;

     }
     return this.productType;

     }
     */

    @Override
    public void setProductType(Product newProduct) {
        productType = newProduct;

    }

    /*
     public Product setProductType(Product newProduct) {
     return newProduct;

     }*/
    /**
     * @return the area
     */
    @Override
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    @Override
    public void setArea(double area) {
        this.area = area;
    }

    /**
     * @return the materialCost
     */
    @Override
    public double getMaterialCost() {
        return materialCost;
    }

    @Override
    public void setMaterialCost() {
        materialCost = this.getProductType().getCostPerSqft() * area;
    }
    /*
     public double setMaterialCost() {
     return this.getProductType().getCostPerSqft() * area;

     }*/

    /**
     * @return the laborCost
     */
    @Override
    public double getLaborCost() {
        return laborCost;
    }

    /**
     * @param laborCost the laborCost to set
     */
    @Override
    public void setLaborCost() {
        laborCost = this.getProductType().getLaborPerSqft() * area;
    }

    /*
     public double setLaborCost() {
     return this.getProductType().getLaborPerSqft() * area;

     }
     */
    @Override
    public double getTotalCost() {
        return totalCost;
    }

    /**
     * @param totalCost the totalCost to set
     */
    @Override
    public void setTotalCost() {
        this.totalCost = materialCost + laborCost + tax;
    }

    /**
     * @return the tax
     */
    @Override
    public double getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    @Override
    public void setTax() {
        this.tax = (laborCost + materialCost) * taxRate;
    }

}
