/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

/**
 *
 * @author Geoffrey & Tai
 */
public class Product implements printInterface, productInterface {

    private String type;
    private double costPerSqft;
    private double laborPerSqft;

    public Product(String typeIn, double costPerSqFt, double laborPerSqFt) {
        type = typeIn;
        this.costPerSqft = costPerSqFt;
        this.laborPerSqft = laborPerSqFt;
    }

    @Override
    public String write() {
        return String.format("%s,%.2f,%.2f", type, costPerSqft, laborPerSqft);
    }

    /**
     * @return the type
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    @Override
    public void setType(String type) {
        this.type = type;
        
    }

    /**
     * @return the costPerSqft
     */
    @Override
    public double getCostPerSqft() {
        return costPerSqft;
    }

    /**
     * @return the laborPerSqft
     */
    @Override
    public double getLaborPerSqft() {
        return laborPerSqft;
    }

}
