/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.dto;

import java.util.Set;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Double.parseDouble;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import orderbook.dao.FileAccess;
import orderbook.operations.Order;
import orderbook.operations.OrderBook;
import orderbook.operations.Product;

/**
 *
 * @author apprentice
 */
public class Controller implements TestProd {

    public orderbook.ui.ConsoleIO console = new orderbook.ui.ConsoleIO();
    public HashMap<String, Double> taxMap = new HashMap<>();
    public HashMap<String, double[]> prodType = new HashMap<>();
    public ArrayList<Order> orderList = new ArrayList();
    public ArrayList<Order> allOrders = new ArrayList();
    public HashMap<String, ArrayList<Order>> newBookOrder = new HashMap();// nbo = converttohash(File);
    public OrderBook newBookList = new OrderBook(newBookOrder);
    public FileAccess readWrite = new FileAccess();
    public final String orderOne = "Order_09222015.txt";

    public double findTaxRate(String state, HashMap<String, Double> taxList) {
        double appliedTax = 0;
        Set<String> key = taxList.keySet();
        for (String K : key) {
            if (K.equalsIgnoreCase(state)) {
                appliedTax = taxList.get(K);
            }
        }
        return appliedTax;
    }

    @Override
    public void Initiate() {

        try {
            //File orders = access.load(orderOne);
            loadFiles();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        boolean cont = true;
        do {

            int userInput = console.readInteger("Please enter your choice: \n"
                    + "1.Display all the order \n"
                    + "2.Add a new order \n"
                    + "3.Remove an order \n"
                    + "4.Edit an order \n" + "5.Save your current work \n"
                    + "6.Exit", 1, 6);

            switch (userInput) {

                case 1:
                    String dateInput = datesList();
                    displayOrders(dateInput);
                    break;

                case 2:
                    addOrder();
                    break;

                case 3:
                    String removeDate = datesList();
                    String removeOrderNum = console.readString("Please enter the number of your order: ");
                    removeOrder(removeDate, removeOrderNum);

                    break;

                case 4:
                    editOrder();
                    break;

                case 5:
                    try {
                        //Simply throw our HashMap to writer method in FileAccess to print to file
                        write();
                        //readWrite.write(newBookOrder);
                    } catch (IOException e) {
                        //had to use try and catch instead of throw because of interface 
                    }
                    break;

                case 6:
                    cont = false;
                    break;
            }

        } while (cont);

    }

    public void loadFiles() throws FileNotFoundException {
        ArrayList<String> fileNames = new ArrayList<>();
        Scanner orderDates = new Scanner(new FileReader("files_to_load.txt"));
        while (orderDates.hasNextLine()) {
            fileNames.add(orderDates.nextLine());
        }
        orderDates.close();
        for (String fileName : fileNames) {
            ArrayList<Order> tempOrderList = new ArrayList<>();
            String g = fileName;
            String gg = null;

            String currentLine;
            ArrayList<String[]> ordersByDate = readWrite.reader(fileName);
            for (String[] splitLineOrders : ordersByDate) {
                if (splitLineOrders.length >= 6) {
                    //handle null pointer
                    Product tempProduct = null;
                    gg = g.substring(6, 14);
                    String orderNum = splitLineOrders[0];
                    String name = splitLineOrders[1];
                    String state = splitLineOrders[2];
                    double taxR = parseDouble(splitLineOrders[3]);
                    String type = splitLineOrders[4];
                    double area = parseDouble(splitLineOrders[5]);
                    // map type to object, create object from the map
                    for (String key : prodType.keySet()) {
                        if (key.equalsIgnoreCase(type)) {
                            double[] costs = prodType.get(key);
                            tempProduct = new Product(key, costs[0], costs[1]);
                        }
                    }

                    Order loadedOrder = new Order(orderNum, name, state, taxR, tempProduct, area);
                    allOrders.add(loadedOrder);

                    tempOrderList.add(loadedOrder);

                } else if (splitLineOrders.length == 2) {
                    taxMap.put(splitLineOrders[0], parseDouble(splitLineOrders[1]));
                } else if (splitLineOrders.length == 3) {
                    double[] costPerSqft = new double[2];
                    costPerSqft[0] = parseDouble(splitLineOrders[1]);
                    costPerSqft[1] = parseDouble(splitLineOrders[2]);
                    prodType.put(splitLineOrders[0], costPerSqft);
                }

            }
            if (gg != null) {
                newBookOrder.put(gg, tempOrderList);
            }
        }
    }

    public void displayOrders(String dateInput) {

        ArrayList<Order> temp = newBookList.findOrderListByDate(dateInput);
        if (temp == null) {
            System.out.println("there is no order on your selection date.");

        } else {
            Iterator<Order> iter = temp.iterator();
            while (iter.hasNext()) {
                Order tempOrder = iter.next();
                System.out.println(tempOrder.write());

            }
        }
    }

    public void addOrder() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
        SimpleDateFormat dateFormatUser = new SimpleDateFormat("MM/dd/yyyy");
        String date = dateFormat.format(cal.getTime());
        String cusName = console.readString("Please enter the customer name: ");

        Order newOrder = new Order(cusName);
        String orderNumIn = newBookList.generateOrderNum(date);
        boolean abb = true;
        String state;
        String stateIn = console.readString("Please enter the abbreviation of the state: ");

        while (stateIn.length() != 2) {
            stateIn = console.readString("Your input is invalid.Please re-enter the abbreviation of the state: ");
        }

        Double taxRateIn = findTaxRate(stateIn, taxMap);

        String type = console.readString("Please enter your option for product type \n"
                + "Carpet \n" + "Laminate \n" + "Tile \n" + "Wood");
        Product tempProduct = null;

        for (String key : prodType.keySet()) {
            if (key.equalsIgnoreCase(type)) {
                double[] costs = prodType.get(key);
                tempProduct = new Product(key, costs[0], costs[1]);
            }
        }

        double areaIn = console.readDouble("Please enter the area for this order: ");

        System.out.println("We have created your order. Your order number: " + orderNumIn);
        if (tempProduct != null) {
            newOrder = new Order(orderNumIn, cusName, stateIn, taxRateIn, tempProduct, areaIn);
        }
        newBookList.addOrder(newOrder, date);
        allOrders.add(newOrder);
    }

    public void removeOrder(String removeDate, String removeOrderNum) {

        newBookList.removeOrder(removeDate, removeOrderNum);

    }

    public void editOrder() {
        String editDate = datesList();

        ArrayList<Order> editList = newBookList.findOrderListByDate(editDate);
        if (editList == null) {
            System.out.println("there is no order on your selection date.");

        } else {

            String editOrderNum = console.readString("Please enter the number of your order: ");
            Order editOrder = newBookList.findOrderByNum(editOrderNum, editList);
            if (editOrder == null) {
                System.out.println("There is no order according to your input order number for " + editDate);

            } else {

                String editName = console.readString("Please enter the new customer name ("
                        + editOrder.getCustomerName() + "): ");
                if (!editName.isEmpty()) {
                    editOrder.setCustomerName(editName);
                }

                String editState = console.readString(("Enter the state (" + editOrder.getState() + ") : "));
                if (!editState.isEmpty()) {
                    while (editState.length() != 2) {
                        editState = console.readString("Your input is invalid.Please re-enter the abbreviation of the state: ");
                    }
                    editOrder.setState(editState);
                    editOrder.setTaxRate(findTaxRate(editState, taxMap));
                    editOrder.setTax();
                    editOrder.setTotalCost();

                }
                //Is this used?
                /*int editProductType = console.readInteger("Choose new product type ("
                 + editOrder.getProductType().getType() + "): \n" + "1.Carpet \n"
                 + "2.Laminate \n" + "3.Tile \n" + "4.Wood ", 1, 4);*/
                String type = console.readString("Please enter your option for product type (" + editOrder.getProductType().getType()
                        + ") \n"
                        + "Carpet \n" + "Laminate \n" + "Tile \n" + "Wood");

                Product tempProduct = null;
                if ("".equals(type)) {
                    type = editOrder.getProductType().getType();
                }

                for (String key : prodType.keySet()) {
                    if (key.equalsIgnoreCase(type)) {
                        double[] costs = prodType.get(key);
                        tempProduct = new Product(key, costs[0], costs[1]);
                    }
                }

                editOrder.setProductType(tempProduct);

                //editOrder.setProductType(editProductType);
                //throw in order setters
                editOrder.setLaborCost();
                editOrder.setMaterialCost();
                editOrder.setTax();
                editOrder.setTotalCost();
                double editArea =0;

                String stringEditArea = console.readString("Please enter the area (" + editOrder.getArea() + "): ");
                if ("".equals(stringEditArea)) {
                    editArea = editOrder.getArea();
                }
                else editArea = Double.parseDouble(stringEditArea);
                editOrder.setArea(editArea);
            }

            editOrder.setLaborCost();
            editOrder.setMaterialCost();
            editOrder.setTotalCost();
            editOrder.setTax();
        }

    }

    public void write() throws IOException {
        Set<String> keySet = newBookOrder.keySet();

        try {
            PrintWriter writeDateFile = new PrintWriter(new FileWriter("files_to_load.txt"));
            writeDateFile.println("Product.txt");
            writeDateFile.println("Tax.txt");
            for (String key : keySet) {

                writeDateFile.println("Order_" + key + ".txt");
                ArrayList<Order> orders = new ArrayList<>();
                orders = newBookOrder.get(key);
                String fileName = "Order_" + key + ".txt";
                readWrite.write(fileName, orders);

            }

            writeDateFile.flush();
            writeDateFile.close();

        } catch (IOException e) {
            System.out.println("Could not save the orders.");
        }
    }

    public String datesList() {
        int i = 0;
        String dates = "";
        for (String k : newBookOrder.keySet()) {
            i += 1;
            dates += k + " ";
        }
        String countDate;
        if (i < 5) {
            countDate = console.readString("Please enter the date of your order: " + dates + " : ");
        } else {
            countDate = console.readString("Please enter the date of your order: ");
        }
        return countDate;
    }

}
