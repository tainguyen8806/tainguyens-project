/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.dto;

import java.io.FileNotFoundException;
import java.io.IOException;
import orderbook.ui.ConsoleIO;

/**
 *
 * @author apprentice
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        ConsoleIO console = new ConsoleIO();
         Controller newController = new Controller();
         Test newTest = new Test();
        
        int mod = console.readInteger("Please enter which mode your want to use today \n"
                + "1.Production \n" + "2.Test ",1,2);
        switch(mod){
            case 1: 
                newController.Initiate();
                break;
            case 2:
                newTest.Initiate();
                break;
                
        }

    }

}
