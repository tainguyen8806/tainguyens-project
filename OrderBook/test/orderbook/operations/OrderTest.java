/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class OrderTest {
    
    public OrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

  
    @Test
    public void testSetProductType_Product() {
        System.out.println("setProductType");
        Product newProduct = new Product("Carpet",2.25,2.1);
        Order instance = new Order("Geofff");
        instance.setProductType(newProduct);
        assertEquals(instance.getProductType(), newProduct);
    }
}


  