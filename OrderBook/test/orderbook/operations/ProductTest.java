/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.operations;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ProductTest {
    
    public ProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of write method, of class Product.
     */
    @Test
    public void testWrite() {
        System.out.println("write");
        Product instance = new Product("Carpet",2,2);
        String expResult = null;
        String expResult2= "Carpet,2.00,2.00";
        String result = instance.write();
        assertFalse(result.equals(expResult));
        assertTrue(result.equals(expResult2));
    }



    /**
     * Test of GetCostPerSqft method, of class Product.
     */
    @Test
    public void testGetCostPerSqft() {
        System.out.println("getCostPerSqft");
        Product instance = new Product("Carpet",2.25,2.10);
        double expResult = 2;
        double result = instance.getCostPerSqft();
        assertEquals(expResult, result,0.25);
    }
//
    /**
     * Test of setLaborPerSqft method, of class Product.
     */
    @Test
    public void testSetLaborPerSqft() {
        System.out.println("getLaborPerSqft");
        Product instance = new Product("Carpet",2.25,2.10);
        double expResult = 2.1;
        double result = instance.getLaborPerSqft();
        assertEquals(expResult, result, 0.0);
    }
    
}
