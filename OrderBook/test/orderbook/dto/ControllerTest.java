/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderbook.dto;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import orderbook.operations.Order;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ControllerTest {
    private Controller instance;
    
    public ControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws FileNotFoundException {
        instance = new Controller();
        instance.loadFiles();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findTaxRate method, of class Controller.
     */
    @Test
    public void testFindTaxRate() throws FileNotFoundException {
        System.out.println("findTaxRate");
        String state = "MN";
        
        //Controller instance = new Controller();
        instance.loadFiles();
        HashMap<String, Double> taxList = instance.taxMap;
        
        double expResult = 0.0719;
        double result = instance.findTaxRate(state, taxList);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of loadFiles method, of class Controller.
     */
    @Test
    
    public void testLoadFiles() throws FileNotFoundException {
        System.out.println("loadFiles");
        //Controller instance = new Controller();
        instance.loadFiles();
    }
    
    @Test
    public void testDisplayOrders() throws FileNotFoundException {
        System.out.println("displayOrders");
        //Controller instance = new Controller();
        instance.loadFiles();
        instance.displayOrders("09222015");
    }
    
    @Test
    public void testRemoveOrder() {
        System.out.println("removeOrder");
        ArrayList<Order> testList = instance.newBookList.findOrderListByDate("09222015");
        int originalSize = testList.size();
        instance.newBookList.removeOrder("09222015", "1");
        int newSize = instance.newBookList.findOrderListByDate("09222015").size();
        assertTrue(originalSize > newSize);
        
        
    }

}
