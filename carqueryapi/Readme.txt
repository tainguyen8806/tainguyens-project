CarqueriAPI project:

- Using carqueriAPI to create a simple java web application which takes a year, make and model as an plain text input and returns which vehicle details for each trim level available for that vehicle.

- I used getTrim methods with input parameter of a keyword.

- CarqueriAPI already handled the difference between 2015 and 15 so I only have to handle '15( which I replace ' with an empty character).

- The only challenge part of this exercise was to understand the mechanism of carqueriAPI. 
