<%-- 
    Document   : carquery
    Created on : Nov 24, 2015, 5:43:25 PM
    Author     : Gon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="http://www.carqueryapi.com/js/jquery.min.js"></script>
        <script type="text/javascript" src="http://www.carqueryapi.com/js/carquery.0.3.4.js"></script>

        <script type="text/javascript">
            $(document).ready(
                    function ()
                    {
                        //Create a variable for the CarQuery object.  You can call it whatever you like.
                        var carquery = new CarQuery();
                        //Run the carquery init function to get things started:
                        carquery.init();
                        $('#search-btn').click(function () {
                            $('#search-results').empty();
                            var ulTest = document.createElement('ul');
                            var string1 = $('#search-String').val();
                            string = string1.replace("'", "");
                            $.getJSON("http://www.carqueryapi.com/api/0.3/" + "?callback=?", {cmd: "getTrims", keyword: string}
                            , function (data) {
                                var trimTest = data.Trims;

                                if (trimTest.length === 0) {
                                    $('#search-results').append('<br>' + "There is no match for your search input. Please check your search input." 
                                            + '<br>' + "Search input should have year, make and model infomation.");
                                } else {
                                    for (var i = 0; i < trimTest.length; i++) {
                                        var d = document.createElement('li');
                                        var e = document.createElement('p');
                                        var f = document.createElement('p');
                                        $(e).html("Engine: " + trimTest[i].model_engine_type + trimTest[i].model_engine_cyl + '<br><p>'
                                                + "Transmission : " + trimTest[i].model_transmission_type + '</p>' + '<p>' + "Body: " + trimTest[i].model_body + '</p>');
                                        $(d).html('<strong>' + trimTest[i].model_year + ' ' + trimTest[i].make_display + ' ' + trimTest[i].model_name + '<br>' + trimTest[i].model_trim + '</strong>');
                                        $(ulTest).append(d).append(e);
                                    }
                                    var divTest = document.createElement('div');
                                    $(divTest).append(ulTest);
                                    $('#search-results').append(divTest);
                                }


                            }
                            );
                        });

                    });


        </script>
        <title>CarQueryAPI Exercise</title>
    </head>
    <body>
        <h1>This is my CarQueryAPI Exercise</h1><br>
        Input <input type="text" id="search-String"/>
        <input id="search-btn" type="button" value="Search"/><br>
        <br>
        <div>
            <div id="search-results"></div>
        </div>

    </body>
</html>
