
import java.util.*;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author apprentice
 */
public class StudentQuizScore {

    public static void main(String[] args) {
        boolean truName = true;

        ArrayList<Integer> studentGrade = new ArrayList<>();

        studentGrade.add(90);
        studentGrade.add(67);
        studentGrade.add(34);
        studentGrade.add(90);
        studentGrade.add(99);

        HashMap<String, ArrayList> mapGrade = new HashMap();

        mapGrade.put("Peter", studentGrade);

        ArrayList<Integer> studentGrade1 = new ArrayList();
        studentGrade1.add(56);
        studentGrade1.add(88);
        studentGrade1.add(67);
        studentGrade1.add(78);
        studentGrade1.add(90);
        mapGrade.put("Ben", studentGrade1);

        ArrayList<Integer> studentGrade2 = new ArrayList();
        studentGrade2.add(99);
        studentGrade2.add(65);
        studentGrade2.add(67);
        studentGrade2.add(56);
        studentGrade2.add(18);
        mapGrade.put("Jack", studentGrade2);

        ArrayList<Integer> studentGrade3 = new ArrayList();
        studentGrade3.add(98);
        studentGrade3.add(81);
        studentGrade3.add(60);
        studentGrade3.add(79);
        studentGrade3.add(40);
        mapGrade.put("You", studentGrade3);

        ArrayList<Integer> studentGrade4 = new ArrayList();
        studentGrade4.add(56);
        studentGrade4.add(88);
        studentGrade4.add(67);
        studentGrade4.add(78);
        studentGrade4.add(90);
        mapGrade.put("You2", studentGrade4);

        ConsoleIO cons = new ConsoleIO();
        int input;

        do {
            input = cons.readInteger(" Please enter your choice \n 1.Add a student \n 2.Remove a student \n"
                    + "3.View the class \n 4. View score from a student \n "
                    + "5.View the average Score \n 6. Exit the program to show average score and min, max", 1, 6);

            if (input == 1) {
                String name = cons.readString("Please enter the student name:");
                int first = cons.readInteger("Please enter the first grade :", 0, 100);
                int second = cons.readInteger("Please enter the second grade :", 0, 100);
                int third = cons.readInteger("Please enter the third grade :", 0, 100);
                int fourth = cons.readInteger("Please enter the fourth grade :", 0, 100);
                int fifth = cons.readInteger("Please enter the fifth grade :", 0, 100);

                ArrayList<Integer> newStudent = new ArrayList();
                newStudent.add(first);
                newStudent.add(second);
                newStudent.add(third);
                mapGrade.put(name, newStudent);
            }

            if (input == 2) {

                do {
                    String name = cons.readString("Please enter the name of student: ");
                    if (mapGrade.remove(name) == null) {
                        truName = false;
                        System.out.println("Cannot find your name input.");

                    } else if (mapGrade.remove(name) != null) {
                        truName = true;
                        mapGrade.remove(name);
                    }

                } while (truName = false);
            }

            if (input == 3) {
                Set<String> key = mapGrade.keySet();
                for (String k : key) {
                    System.out.println(k + " " + mapGrade.get(k));

                }

            }
            if (input == 4) {
                do {
                    String name = cons.readString("Please enter the student name: ");
                    Set<String> key = mapGrade.keySet();
                    for (String k : key) {
                        if (k.equals(name)) {
                            System.out.println(k + " " + mapGrade.get(k));

                        } else if (k != name) {
                            truName = false;
                        }
                    }
                } while (truName);

                if (truName = false) {
                    System.out.println("Cannot find the student");
                }

            }
            if (input == 5) {
                double total = 0, average = 0;
                int count = 0;
                String name = cons.readString("Please enter the student name: ");
                Set<String> key = mapGrade.keySet();
                for (String k : key) {
                    if (k.equals(name)) {
                        ArrayList<Integer> subGrade = mapGrade.get(k);
                        for (int i = 0; i < subGrade.size(); i++) {
                            total += subGrade.get(i);
                            count++;

                        }

                    }
                }
                average = total / count;
                System.out.println("The average score of " + name + " is " + average);

            }
        } while (input != 6);

        System.out.println("=============================");

        Set<String> key = mapGrade.keySet();
        double total = 0, average = 0, classAverage = 0;
        int count = 0, min = 100, max = 0;
        String nameMax = "", nameMin = "";
        ArrayList<String> maxStudent = new ArrayList();
        ArrayList<String> minStudent = new ArrayList();

        for (String k : key) {
            ArrayList<Integer> subGrade = mapGrade.get(k);
            for (int i = 0; i < subGrade.size(); i++) {
                total += subGrade.get(i);
                count++;
                if (subGrade.get(i) > max) {
                    max = subGrade.get(i);
                    nameMax = k;
                }
                if (subGrade.get(i) < min) {
                    min = subGrade.get(i);
                    nameMin = k;
                }
            }
        }

        for (String k2 : key) {
            ArrayList<Integer> subGrade2 = mapGrade.get(k2);
            for (int a = 0; a < subGrade2.size(); a++) {
                if (max == subGrade2.get(a)) {
                    maxStudent.add(k2);
                }

            }
        }
        for (String k3 : key) {
            ArrayList<Integer> subGrade3 = mapGrade.get(k3);
            for (int a = 0; a < subGrade3.size(); a++) {
                if (min == subGrade3.get(a)) {
                    minStudent.add(k3);
                }

            }
        }

        average = total / count;

        System.out.println(
                "The class average score is " + average);
        System.out.println(
                "Your max score is " + max + " and student name are " + maxStudent);
        System.out.println(
                "Your min score is " + min + " and student name are " + minStudent);

        System.out.println(
                "===============================");

    }
}
