/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Operation.Player;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class FileAccess {

    public ArrayList<String[]> readFile(String fileName) throws Exception {
        int i = 0;
        ArrayList<String[]> newList = new ArrayList<>();
        Scanner kb = new Scanner(new BufferedReader(new FileReader(fileName)));
        String current;
        String[] splitLine;
        while (kb.hasNextLine()) {
            current = kb.nextLine();
            splitLine = current.split("::");
            newList.add(i, splitLine);
            i++;

        }
        kb.close();

        return newList;
    }

    public void writeFile(String fileName, HashMap<String, ArrayList<Player>> teamMap) throws Exception {
        PrintWriter kb = new PrintWriter(new FileWriter(fileName));
        String writeString;
        Set<String> key = teamMap.keySet();
        for (String k : key) {
            ArrayList<Player> playerList = teamMap.get(k);
            for (Player k1 : playerList) {
                writeString = k1.getName() + "::" + k1.getTeamName();
                kb.println(writeString);
                writeString = ("");
            }

        }
        kb.flush();
        kb.close();

    }
}
