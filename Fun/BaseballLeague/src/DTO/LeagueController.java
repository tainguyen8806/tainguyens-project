/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DAO.FileAccess;
import Operation.League;
import Operation.Player;
import Operation.Team;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class LeagueController {

    ArrayList<Player> newRoster = new ArrayList<>();

    HashMap<String, ArrayList<Player>> League = new HashMap();
    League newLeague = new League(League);
    UI.ConsoleIO Console = new UI.ConsoleIO();
    String teamName;
    Team newTeam = new Team(teamName);
    FileAccess newAccess = new FileAccess();

    public void Initiate() throws Exception {

        loadFile(newAccess.readFile("League.txt"));
        boolean test = true;
        if (League == null) {

            System.out.println("league is null");
        } else {
            System.out.println("League is not null");
        }

        boolean cont = true;

        do {
            int input = Console.readInteger("Please enter your option: \n"
                    + "1. Create new team \n" + "2. Create new player. \n"
                    + "3. List all the player in the league. \n"
                    + "4. List all the player in a team. \n"
                    + "5. Trade a player. \n"
                    + "6. Delete a player. \n" + "7. Exit the program", 1, 7);

            switch (input) {

                case 1:
                    addTeam();

                    break;

                case 3:
                    listAll();
                    break;

                case 4:
                    listTeamPlayer();
                    break;
                case 5:
                    tradePlayer();

                    break;

                case 6:
                    deletePlayer();
                    break;

                case 7:
                    newAccess.writeFile("newLeague.txt", League);
                    cont = false;
                    break;
            }

        } while (cont);
    }

    public void loadFile(ArrayList<String[]> loadList) throws Exception {

        ArrayList<String[]> loadFileList = newAccess.readFile("League.txt");
        for (String[] k : loadFileList) {

            ArrayList<Player> newArrayList = new ArrayList<>();
            League.put(k[1], newArrayList);
        }

        for (String[] k : loadFileList) {
            Set<String> key = League.keySet();
            for (String kk : key) {
                if (kk.equalsIgnoreCase(k[1])) {
                    ArrayList<Player> oldList = League.get(kk);
                    Player newAddPlayer = new Player(k[1]);
                    newAddPlayer.setName(k[0]);
                    oldList.add(newAddPlayer);
                    League.put(kk, oldList);

                }

            }
        }
    }

    public void addTeam() {
        String teamName = Console.readString("Please enter the name of the new team: ");
        Set<String> key = League.keySet();
        ArrayList<Player> newTeam = new ArrayList();
        for (String k : key) {
            if (k.equalsIgnoreCase(teamName)) {
                System.out.println(teamName + " is alreasy existed in your League.");
                break;
            }
        }
        League.put(teamName, newTeam);
    }

    public void addPlayer() {
        String findTeamName = Console.readString("Please enter the name of the team to create new player: ");
        if (newLeague.findTeamNameBoolean(findTeamName)) {
            Set<String> key2 = League.keySet();
            for (String k2 : key2) {
                if (k2.equalsIgnoreCase(findTeamName)) {
                    findTeamName = k2;

                    Player addPlayer = new Player(findTeamName);

                    ArrayList<Player> addPlayerList = newLeague.findTeamByName(findTeamName);
                    String addName = Console.readString("Please enter the name of new player: ");
                    addPlayer.setName(addName);
                    addPlayerList.add(addPlayer);
                    League.put(findTeamName, addPlayerList);
                }
            }
        }
    }

    public void listAll() {
        Set<String> key1 = League.keySet();
        for (String k : key1) {
            ArrayList<Player> allList = League.get(k);
            for (Player playerList : allList) {
                System.out.println(playerList.getName() + ":: " + playerList.getTeamName());
            }
        }
    }

    public void listTeamPlayer() {
        String tempTeamName = Console.readString("Please enter the team name: ");
        if (newLeague.findTeamNameBoolean(tempTeamName)) {
            Set<String> tempKey = League.keySet();
            for (String k : tempKey) {
                if (k.equalsIgnoreCase(tempTeamName)) {
                    if (League.get(k).size() == 0) {
                        System.out.println("There is currently no players in " + tempTeamName + "'s roster");
                        break;
                    } else {
                        for (Player a : League.get(k)) {
                            System.out.println(a.getName() + "::" + a.getTeamName());
                        }
                        break;

                    }
                }

            }
        }

    }

    public void tradePlayer() {
        boolean t = false;
        String findTeam = Console.readString("Please enter the name of the team to trade player: ");
        if (newLeague.findTeamNameBoolean(findTeam)) {

            ArrayList<Player> tradeList = new ArrayList();
            Set<String> key2 = League.keySet();
            for (String k2 : key2) {
                if (k2.equalsIgnoreCase(findTeam)) {
                    findTeam = k2;
                    tradeList = League.get(k2);
                }
            }
            String playerName = Console.readString("Please enter the name of the player to trade: ");
            for (int i = 0; i < tradeList.size(); i++) {
                if (tradeList.get(i).getName().equalsIgnoreCase(playerName)) {
                    t = true;
                    playerName = tradeList.get(i).getName();
                    String teamTrade = Console.readString("Please enter the name of the team to trade : " + playerName);
                    if (newLeague.findTeamNameBoolean(teamTrade)) {
                        for (String k : key2) {
                            if (k.equalsIgnoreCase(teamTrade)) {
                                teamTrade = k;
                                Player tradePlayer = tradeList.get(i);
                                tradePlayer.setTeamName(teamTrade);
                                ArrayList<Player> tradeTeam = newLeague.findTeamByName(teamTrade);
                                tradeTeam.add(tradePlayer);
                                League.put(teamTrade, tradeTeam);
                                tradeList.remove(i);
                                League.put(findTeam, tradeList);
                                break;
                            }
                        }
                    }
                    break;
                }

            }
            if (t == false) {
                System.out.println("Cannot find " + playerName);

            }
        }
    }

    public void deletePlayer() {
        boolean find = false;
        String delTeam = Console.readString("Please enter the name of the team to delete player: ");
        ArrayList<Player> delList = newLeague.findTeamByName(delTeam);
        if (delList == null) {
            System.out.println("Cannot find the team to delete player.");

        } else if (delList.size() == 0) {
            System.out.println(delTeam + "'s roster does not have any player at this point.");

        } else {
            String delPlayer = Console.readString("Please enter the name of the player to delete: ");
            for (int i = 0; i < delList.size(); i++) {
                if (delList.get(i).getName().equalsIgnoreCase(delPlayer)) {
                    delList.remove(i);
                    System.out.println("Successfully delete " + delPlayer);
                    find = true;
                    break;

                }
            }

            if (find == false) {
                System.out.println("Cannot find the player to delete.");
            }
        }
    }
}
