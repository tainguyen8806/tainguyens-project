/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

/**
 *
 * @author apprentice
 */
public class Player {

    private String name;
    private String teamName;

    public Player(String teamNameIn) {
        teamName = teamNameIn;
    }

    public String toString() {
        return name + " is playing for " + teamName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the newTeam
     */
    public String getTeamName() {
        return teamName;
    }
    
    public void setTeamName(String name){
        teamName = name;
    }
}

    
