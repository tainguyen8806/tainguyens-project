/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class Team {

    private String name;
    private ArrayList<Player> Team;

    public Team(String teamName) {
        name = teamName;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public void add(Player newPlayer) {
        Team.add(newPlayer);
    }

    public void delete(Player deletePlayer) {
        Team.remove(deletePlayer);
    }
    
        
    }


    
