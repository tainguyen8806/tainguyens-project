/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class League {

    private HashMap<String, ArrayList<Player>> league;

    public League(HashMap<String, ArrayList<Player>> newLeague) {
        league = newLeague;
    }

    ;
    
    public ArrayList<Player> findTeamByName(String teamName) {
        ArrayList<Player> foundTeam = null;
        Set<String> key = league.keySet();
        for (String k : key) {
            if (k.equalsIgnoreCase(teamName)) {
                foundTeam = league.get(k);
            }

        }
        return foundTeam;
    }

    public boolean findTeamNameBoolean(String Name) {
        boolean find = true;
        Set<String> key = league.keySet();
        for (String k : key) {
            if (k.equalsIgnoreCase(Name)) {
                find = true;
                return find;
            }
        }
        find = false;
        System.out.println(Name + " is not existed in our League.");
        return find;
    }

}
