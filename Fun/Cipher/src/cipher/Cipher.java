/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cipher;

import java.util.*;

/**
 *
 * @author apprentice
 */
public class Cipher {

    /**
     * @param args the command line arguments
     * @return
     */
    public static String ceasarCipher() {

        Scanner kb = new Scanner(System.in);

        // TODO code application logic here
        char[] alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z'};

        System.out.println("Please enter the String you would like to encrypt today: ");
        String userIn = kb.nextLine();
        String result = "";
        String[] userSplit = userIn.split(" ");

//        System.out.println(alpha.length);
        for (int a = 0; a < userSplit.length; a++) {
            StringBuilder encrypted = new StringBuilder();

            for (int i = 0; i < userSplit[a].length(); i++) {
                for (int j = 0; j < alpha.length; j++) {

                    if (userSplit[a].charAt(i) == alpha[j]) {
                        if (j > 21) {
                            int c = 4 - (alpha.length - j);
                            char add = alpha[c];
                            encrypted.append(add);
                        } else {
                            char add = alpha[j + 4];
                            encrypted.append(add);
                        }

                    }

                }

            }
            result += encrypted + " ";

        }
        return result;
    }

    public static String ceasarCipherDec() {
        Scanner kb = new Scanner(System.in);

        // TODO code application logic here
        char[] alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z'};

        System.out.println("Please enter the String you would like to decrypt today: ");
        String userIn = kb.nextLine();
        String result = "";
        String[] userSplit = userIn.split(" ");
//        System.out.println(alpha.length);
        for (int a = 0; a < userSplit.length; a++) {
            StringBuilder encrypted = new StringBuilder();

            for (int i = 0; i < userSplit[a].length(); i++) {
                for (int j = 0; j < alpha.length; j++) {

                    if (userSplit[a].charAt(i) == alpha[j]) {
                        if (j < 4) {
                            int c = alpha.length - (4 - j);
                            char add = alpha[c];
                            encrypted.append(add);
                        } else {
                            char add = alpha[j - 4];
                            encrypted.append(add);
                        }

                    }

                }

            }
            result += encrypted + " ";

        }
        return result;

    }

    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);
        boolean cont = true;

        do {
            System.out.println("What would you like to do today: \n"
                    + "1. Encrypte a message \n"
                    + "2. Decrypte a message \n"
                    + "3. Exit the program ");

            try {
                int userChoice = kb.nextInt();
                if (userChoice < 1 || userChoice > 3) {
                    System.out.println("Value must be between 1 and 3");
                } else {
                    switch (userChoice) {
                        case 1:
                            String result = ceasarCipher();
                            System.out.println(result);
                            break;

                        case 2:
                            result = ceasarCipherDec();
                            System.out.println(result);
                            break;

                        case 3:
                            System.out.println("Thank you for trying my cipher program");
                            cont = false;
                            break;

                    }
                }
            } catch (InputMismatchException e) {
                System.out.println("Input must be an integer");
                cont = true;
                kb.nextLine();

            }
            

        }while (cont);

    }
}
