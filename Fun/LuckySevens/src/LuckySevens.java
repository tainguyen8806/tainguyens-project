
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    public static void main(String[] args) {
        Scanner KB = new Scanner(System.in);
        int Dice1, Dice2, betMoney, Result;
        int moneyMax = 0, rollCount, rollCountMax = 0;

        System.out.print("How much would you like to play to day: ");
        betMoney = KB.nextInt();
        while (betMoney <= 0) {
            System.out.print("You cannot play with that money Sir.");
            System.out.print("Please enter your bet amount: ");
            betMoney = KB.nextInt();
        }

        for (rollCount = 1; betMoney > 0; rollCount++) {
            Dice1 = (int) (Math.random() * 6) + 1;
            Dice2 = (int) (Math.random() * 6) + 1;
            Result = Dice1 + Dice2;

            if (Result == 7) {
                betMoney += 4;
            } else {
                betMoney -= 1;
            }
            if (betMoney > moneyMax) {
                moneyMax = betMoney;
                rollCountMax = rollCount;

            }
        }

        System.out.println("You are broke after " + rollCount);
        System.out.println("You shoud have quit after " + rollCountMax + " rolls when you had " + moneyMax);
    }
}
