/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import Operations.Checking;
import Operations.Saving;
import UI.ConsoleIO;
import Operations.Account;

/**
 *
 * @author apprentice
 */
public class ATMcontroller {

    public void Initiate() {

        Checking newChecking = new Checking("tainguyenAcc");
        Saving newSaving = new Saving("tainguyenAcc");
        ConsoleIO console = new ConsoleIO();
        boolean cont = true;

        String input = console.readString("Please enter your PIN: ");

        if (input.equalsIgnoreCase(newChecking.getPIN())) {

            do {
                int accInput = console.readInteger("Please select your account (1: Saving 2: Checking 3: Exit): ", 1, 3);

                if (accInput == 1) {
                    System.out.println("Your saving account balance is : " + newSaving.getSavingBalance());
                    int savingInput = console.readInteger("Please enter your desire operation: \n"
                            + "1.Deposit \n" + "2.Withdraw \n" + "3.Account Balance \n" + "4. Exit", 1, 4);
                    switch (savingInput) {

                        case 1:
                            int savingDeposit = console.readInteger("Please enter the amount to deposit: ");
                            if (savingDeposit >= 10000) {
                                System.out.println("Your transaction cannot be completed. Please contact account manager directly");
                                break;
                            } else if (savingDeposit > 0) {
                                newSaving.setSavingDeposit(savingDeposit);
                                newSaving.deposit();
                                System.out.println("Your transaction has been completed. Your balance now is : " + newSaving.getSavingBalance());
                            } else {
                                System.out.println("Your input is invalid. Please try again");
                            }
                            break;

                        case 2:
                            int savingWithdraw = console.readInteger("Please enter the amount to withdraw: ");
                            if (newSaving.getSavingBalance() >= 0 && newSaving.getSavingBalance() >= savingWithdraw) {
                                newSaving.setSavingWithdraw(savingWithdraw);
                                newSaving.withDraw();
                                System.out.println("Your transaction has been completed. Your current saving balance is: " + newSaving.getSavingBalance());
                                break;
                                /*if (newSaving.getSavingBalance()< 0){
                                 newSaving.applyFee();
                                 System.out.println("Your transaction has been completed. \n" +
                                 "You have overdrafted your account. $10 fee has been applied to your balance");
                                 System.out.println("Your balance now is : " + newSaving.getSavingBalance());
                                 }
                                 break;
                                 } else {
                                 System.out.println("Your current balance is negative. Withdrawing funds from your account is unavailable");
                                 break;*/
                            } else if (savingWithdraw > newSaving.getSavingBalance()) {
                                System.out.println("Insufficient funds in your account. Please contact account manager or making deposit.");
                                break;
                            }
                        case 3:
                            System.out.println("Your current saving balance is : " + newSaving.getSavingBalance());
                            break;

                        case 4:
                            System.out.println("Thank you very much for using our service. Have a wonderful day!!!");
                            break;

                    }

                } else if (accInput == 2) {
                    System.out.println("Your checking account balance is: " + newChecking.getCheckingbalance());
                    int checkingInput = console.readInteger("Please enter your desire operation: \n"
                            + "1.Deposit \n" + "2.Withdraw \n" + "3.Account Balance \n" + "4. Exit", 1, 4);
                    switch (checkingInput) {
                        case 1:
                            int checkingDeposit = console.readInteger("Please enter the amount to deposit: ");
                            if (checkingDeposit > 10000) {
                                System.out.println("Your transaction cannot be completed. Please contact account manager directly");
                                break;

                            } else if (checkingDeposit > 0) {
                                newChecking.setCheckingDeposit(checkingDeposit);
                                newChecking.deposit();
                                System.out.println("Your transaction has been completed. Your current checking balance is: " + newChecking.getCheckingbalance());
                                break;
                            } else {
                                System.out.println("Your input is invalid. Please try again.");
                            }
                            break;

                        case 2:
                            if (newChecking.getCheckingbalance() > 0) {
                                int checkingWithdraw = console.readInteger("Please enter the amount to withdraw: ");
                                newChecking.setCheckingWithdraw(checkingWithdraw);
                                newChecking.withDraw();
                                if (newChecking.getCheckingbalance() < 0 && newChecking.getCheckingbalance() > -100) {
                                    newChecking.applyFee();
                                    System.out.println("Your transaction has been completed. \n"
                                            + "You have overdrafted your checking account. $10 fee has been applied to your checking balance. \n"
                                            + "Your current balance is: " + newChecking.getCheckingbalance());
                                    break;

                                } else {
                                    System.out.println("Your current checking balance is: " + newChecking.getCheckingbalance());
                                }
                                break;
                            } else {
                                System.out.println("Your checking balance is negative. Please make a deposit or contact account manager.");
                            }
                            break;

                        case 3:
                            System.out.println("Your current checking account balance is: " + newChecking.getCheckingbalance());
                            break;

                        case 4:
                            System.out.println("Thank you very much for using our service. Have a wonderful day!!!");
                            break;
                    }
                } else if (accInput == 3) {
                    cont = false;
                }

            } while (cont);

        } else {
            System.out.println("Your PIN is invalid. Please re-run the program");
        }
    }
}
