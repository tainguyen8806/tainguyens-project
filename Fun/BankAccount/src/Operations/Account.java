package Operations;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public abstract class Account {

    private String accountNumber;
    protected double balance = 0;
    private String PIN = "1234";

    public Account(String accountNumIn) {
        accountNumber = accountNumIn;

    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    public abstract double withDraw();
    
    
    public abstract double deposit();

    /**
     * @return the PIN
     */
    public String getPIN() {
        return PIN;
    }
    
}

   