/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operations;

/**
 *
 * @author apprentice
 */
public class Checking extends Account {

    private double checkingBalance;
    private double checkingWithdraw;
    private double checkingDeposit;

    public Checking(String accountNumIn) {
        super("accountNum");
        checkingBalance = 200;
    }

    @Override
    public double withDraw() {
        return checkingBalance -= checkingWithdraw;

    }

    @Override
    public double deposit() {
        return checkingBalance += checkingDeposit;
    }

    /**
     * @return the checkingWithdraw
     */
    public double getCheckingWithdraw() {
        return checkingWithdraw;
    }

    /**
     * @param checkingWithdraw the checkingWithdraw to set
     */
    public void setCheckingWithdraw(double checkingWithdraw) {
        this.checkingWithdraw = checkingWithdraw;
    }

    /**
     * @return the checkingDeposit
     */
    public double getCheckingDeposit() {
        return checkingDeposit;
    }

    /**
     * @param checkingDeposit the checkingDeposit to set
     */
    public void setCheckingDeposit(double checkingDeposit) {
        this.checkingDeposit = checkingDeposit;
    }

    /**
     * @return the checkingBalance
     */
    public double getCheckingbalance() {
        return checkingBalance;
    }
    
    public double applyFee(){
        return checkingBalance -=10;
    }

}
