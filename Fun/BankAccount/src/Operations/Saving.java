/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operations;

/**
 *
 * @author apprentice
 */
public class Saving extends Account{
    private double savingBalance ;
    private double savingDeposit;
    private double savingWithdraw;
    
    public Saving(String accountNum){
        super("accountNumber");
        savingBalance = 200;
    }

    
    @Override
    public double withDraw(){
        return savingBalance -= savingWithdraw;
    }
    
    @Override
    public double deposit(){
        return savingBalance +=savingDeposit;
    }

    /**
     * @return the savingDeposit
     */
    public double getSavingDeposit() {
        return savingDeposit;
    }

    /**
     * @param savingDeposit the savingDeposit to set
     */
    public void setSavingDeposit(double savingDeposit) {
        this.savingDeposit = savingDeposit;
    }

    /**
     * @return the savingWithdraw
     */
    public double getSavingWithdraw() {
        return savingWithdraw;
    }

    /**
     * @param savingWithdraw the savingWithdraw to set
     */
    public void setSavingWithdraw(double savingWithdraw) {
        this.savingWithdraw = savingWithdraw;
    }

    /**
     * @return the savingBalance
     */
    public double getSavingBalance() {
        return savingBalance;
    }
    
    
}
