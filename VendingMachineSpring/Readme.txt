Fully function web application from front end to back end:

-Application stimulates a simple vending machine: user insert money , purchase a product and receive change.

-Very similar to DvdLibrary project but I applying more function such as manipulating data through MySql query and JavaScript.

-Decrease the count of a product by 1 when a purchase is success from user through MySql query.

-Remove the product from the list of option when an item count reaches 0 through MySql query.

-Prompt alert box if the money insert is 0 or less then item price.
	
-Prompt alert box to confirm the purchase of the specific item. 

MySQL script to create the database:

*-------------------------------------
Create database called "vending";

SET SQL_MODE= "NO_AUTO_VALUE_ON_ZERO";

SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `item`(

`item_id` int(10) NOT NULL AUTO_INCREMENT,
`item_name` varchar(50) NOT NULL,
`item_price` decimal(4,0) NOT NULL,
`item_count` int(2) NOT NULL,
PRIMARY KEY (`item_id`)
) ENGINE =InnoDB DEFAULT CHARSET=latin1 auto_increment=1;

-----------------------------------*
Create database called "vending_test";

CREATE TABLE IF NOT EXISTS `item`(

`item_id` int(10) NOT NULL AUTO_INCREMENT,
`item_name` varchar(50) NOT NULL,
`item_price` decimal(4,0) NOT NULL,
`item_count` int(2) NOT NULL,
PRIMARY KEY (`item_id`)
) ENGINE =InnoDB DEFAULT CHARSET=latin1 auto_increment=1;

----------------------------------*



