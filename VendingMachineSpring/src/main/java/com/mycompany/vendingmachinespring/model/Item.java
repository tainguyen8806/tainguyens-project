/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinespring.model;

import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Item {
    private int itemId;
    
    @NotEmpty(message="You must supply the name for this item")
    @Length(max=50 , message="Item name must be no more than 50 character in length")
    private String itemName;
    
    @NotNull(message="You must supply the price for this item")
    @Min(value=(long) 0.01,message="Item price must be more than 1 cent")
    private BigDecimal itemPrice;
    
    @Min(value=1, message="You must supply the valid input for this item count")
    private int itemCount;

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * @return the itemPrice
     */
    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    /**
     * @param itemPrice the itemPrice to set
     */
    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    /**
     * @return the itemCount
     */
    public int getItemCount() {
        return itemCount;
    }

    /**
     * @param itemCount the itemCount to set
     */
    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.itemId;
        hash = 37 * hash + Objects.hashCode(this.itemName);
        hash = 37 * hash + Objects.hashCode(this.itemPrice);
        hash = 37 * hash + Objects.hashCode(this.itemCount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (this.itemId != other.itemId) {
            return false;
        }
        if (!Objects.equals(this.itemName, other.itemName)) {
            return false;
        }
//        if (this.itemPrice.doubleValue() != other.itemPrice.doubleValue()) {
//            return false;
//        }
        if (this.itemCount != other.itemCount) {
            return false;
        }
        return true;
    }

    /**
     * @return the itemId
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * @param itemId the itemId to set
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
    
    
}
