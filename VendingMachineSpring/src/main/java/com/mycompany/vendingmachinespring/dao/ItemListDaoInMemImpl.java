/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinespring.dao;

import com.mycompany.vendingmachinespring.model.Item;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class ItemListDaoInMemImpl implements ItemListDao {
    
    private Map<Integer, Item> itemMap = new HashMap<>();
    private static int itemIdCounter = 0;

    @Override
    public Item addItem(Item item) {
        item.setItemId(itemIdCounter);
        itemIdCounter++;
        itemMap.put(item.getItemId(), item);
        return item;
    }

    @Override
    public void removeItem(int itemId) {
        itemMap.remove(itemId);
    }

    @Override
    public void updateItem(Item item) {
        item.setItemCount(item.getItemCount()-1);
        itemMap.put(item.getItemId(), item);
    }

    @Override
    public List<Item> getAllItems() {
        Collection<Item> c = itemMap.values();
        return new ArrayList(c);
    }

    @Override
    public Item getItemById(int itemId) {
        return itemMap.get(itemId);
    }

    
}
