/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinespring.dao;

import com.mycompany.vendingmachinespring.model.Item;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class ItemListDaoDbImpl implements ItemListDao {

    private final static String SQL_INSERT_ITEM
            = "insert into item (item_name, item_price, item_count) values (?,?,?)";

    private final static String SQL_DELETE_ITEM
            = "delete from item where item_id=?";
    
    private final static String SQL_DELETE_EMPTY_ITEM
            ="delete from item where item_count =0";

    private final static String SQL_SELECT_ITEM
            = "select * from item where item_id = ?";
    private final static String SQL_SELECT_ALL_ITEMS
            = "select * from item order by item_name";

    private final static String SQL_UPDATE_ITEM
            = "update item set item_name = ?, item_price = ? , item_count = ?-1 where  item_id = ?";

    private JdbcTemplate jdbcTemplate;

    public void setjdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static final class ItemMapper implements ParameterizedRowMapper<Item> {

        @Override
        public Item mapRow(ResultSet rs, int i) throws SQLException {
            Item item = new Item();
            item.setItemId(rs.getInt("item_id"));
            item.setItemName(rs.getString("item_name"));
            item.setItemPrice(rs.getBigDecimal("item_price"));
            item.setItemCount(rs.getInt("item_count"));

            return item;

        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Item addItem(Item item) {
        jdbcTemplate.update(SQL_INSERT_ITEM,
                item.getItemName(),
                item.getItemPrice(),
                item.getItemCount());
        item.setItemId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return item;

    }

    @Override
    public void removeItem(int item_id) {
        jdbcTemplate.update(SQL_DELETE_ITEM, item_id);
    }

    @Override
    public void updateItem(Item item) {

        jdbcTemplate.update(SQL_UPDATE_ITEM,
                item.getItemName(),
                item.getItemPrice(),
                item.getItemCount(),
                item.getItemId());
        jdbcTemplate.update(SQL_DELETE_EMPTY_ITEM);
    }

    @Override
    public List<Item> getAllItems() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ITEMS, new ItemMapper());
    }

    @Override
    public Item getItemById(int item_Id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ITEM, new ItemMapper(), item_Id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

}
