/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinespring.dao;

import com.mycompany.vendingmachinespring.model.Item;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface ItemListDao {
    
    public Item addItem(Item item);
// remove the Item with the given id from the data store

    public void removeItem(int itemId);
// update the given Item in the data store

    public void updateItem(Item item);
// retrieve all Items from the data store

    public List<Item> getAllItems();
// retrieve the Item with the given id from the data store

    public Item getItemById(int itemId);
// search for Items by the given search criteria values

//    public List<Item> searchItems(Map<SearchTerm, String> criteria);
    
}
