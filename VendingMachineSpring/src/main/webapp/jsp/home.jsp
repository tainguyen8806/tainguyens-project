<%-- 
    Document   : home
    Created on : Oct 22, 2015, 1:10:53 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vending Machine</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <h1 class="text-center">Vending Machine</h1>
        <div class="navbar-default">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                    <a href="${pageContext.request.contextPath}/home">Home</a>
                </li>
                <li role="presentation">
                    <a href="${pageContext.request.contextPath}/add">Add Item</a>
                </li>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7"><h2 class="col-md-offset-4">Item List</h2>
                    <table id="dvdTable" class="table table-hover">
                        <tr>
                            <th width="35%">Name</th>
                            <th width="20%">Price</th>
                            <th width="20%">Count</th>
                            <th width="45%"></th>
                        </tr>
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>

                <div class="col-md-5">
                    <h2 class="text-center">User Input</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="amount-insert" class="col-md-4 control-label">
                                Amount Insert:
                            </label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" id="add-money" placeholder="add money"/>
                            </div>
                        </div>
                    </form>
                    <table class="table" id="amount-change">
                        <tr>
                            <th class="col-md-4">Amount Change:</th>
                        </tr>
                        <tbody id="changeRows"></tbody>
                    </table>
                </div>

            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/home.js"></script>
    </body>
</html>
