<%-- 
    Document   : add
    Created on : Oct 22, 2015, 4:22:05 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body background= "coke.png">
        <h1 class="text-center">Vending Machine</h1>
        <div class="navbar-default">
            <ul class="nav nav-tabs">
                <li role="presentation">
                    <a href="${pageContext.request.contextPath}/home">Home</a>
                </li>
                <li role="presentation" class="active">
                    <a href="${pageContext.request.contextPath}/add">Add Item</a>
                </li>
        </div>
        <div class="col-md-6">
            <h2 class="col-md-offset-6">Add new Item</h2><br>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="add-Item" class="col-md-4 control-label">
                        Name:
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-name" placeholder="Name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-item-price" class="col-md-4 control-label">
                        Item Price:
                    </label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="add-item-price"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-item-count" class="col-md-4 control-label">
                        Item Count:
                    </label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="add-item-count"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-default">
                            Add Item
                        </button>
                    </div>
                </div>
            </form>
            <div id="validationErrors" style="color:red"/> 
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/home.js"></script>
</body>
</html>
