/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    loadItems();

});

function clearItemsTable() {
    $('#contentRows').empty();
}

function clearChangeTable() {
    $('#changeRows').empty();
}

$('#add-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'item',
        data: JSON.stringify({
            itemName: $('#add-name').val(),
            itemPrice: $('#add-item-price').val(),
            itemCount: $('#add-item-count').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#add-name').val('');
        $('#add-item-price').val('');
        $('#add-item-count').val('');
        $('#validationErrors').empty();
        loadItems();
    }).error(function (data, status) {
        $('#validationErrors').empty();
        $.each(data.responseJSON.fieldErrors, function (index, validationError) {
            $('#validationErrors').append(validationError.message).append($('<br>'));
        });
    });
});


function loadItems() {
    clearItemsTable();
    var iTable = $('#contentRows');
    $.ajax({
        url: 'items'
    }).success(function (data, status) {
        $.each(data, function (index, item) {
            iTable.append($('<tr>')
                    .append($('<td>').text(item.itemName))
                    .append($('<td>').text(item.itemPrice))
                    .append($('<td>').text(item.itemCount))
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'orderItem(' +
                                                item.itemId + ')'
                                    })
                                    .text("Purchase")
                                    )
                            )
                    );
        });
    });
}
function orderItem(id) {
    $.ajax({
        type: 'GET',
        url: 'item/' + id
    }).success(function (item) {
        if (item.itemCount <= 0) {
            alert("We are currently out of this item for now. Sorry for the inconvenient!!!");
        } else {
            if ($('#add-money').val() < item.itemPrice) {
                alert("Please insert your money");
            } else {
                var answer = confirm("Do you really want to purchase this item?");
                if (answer === true) {
                    $.ajax({
                        type: 'PUT',
                        url: 'item/' + id,
                        data: JSON.stringify({
                            itemId: id,
                            itemName: item.itemName,
                            itemPrice: item.itemPrice,
                            itemCount: item.itemCount
                        }),
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        'dataType': 'json'
                    }).success(function () {
                        message = alert("You have successfully purchase this item");
                        clearChangeTable();
                        var ctable = $('#changeRows');
                        ctable.append($('#add-money').val() - item.itemPrice);
                        $('#add-money').val('');
                        loadItems();
                    });
                }
            }
        }

    });
}
;






