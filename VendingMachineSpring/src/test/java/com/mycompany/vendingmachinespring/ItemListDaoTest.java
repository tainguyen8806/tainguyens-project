/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinespring;

import com.mycompany.vendingmachinespring.dao.ItemListDao;
import com.mycompany.vendingmachinespring.model.Item;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class ItemListDaoTest {

    private ItemListDao dao;

    public ItemListDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("itemListDao",ItemListDao.class);
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from item");
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void addGetDeleteItem(){
        Item a = new Item();
        a.setItemName("M&M");
        a.setItemPrice(BigDecimal.valueOf(2.5));
        a.setItemCount(2);
        dao.addItem(a);
        Item fromDb = dao.getItemById(a.getItemId());
        assertEquals(fromDb,a);
        dao.removeItem(a.getItemId());
        assertNull(dao.getItemById(a.getItemId()));
        
    }
//    
    @Test
    
    public void addUpdateItem(){
        Item a = new Item();
        a.setItemName("Candy");
        a.setItemPrice(BigDecimal.valueOf(2));
        a.setItemCount(2);
        dao.addItem(a);
        a.setItemCount(3);
        Item fromDb = dao.getItemById(a.getItemId());
        assertEquals(fromDb,a);
    }
}
