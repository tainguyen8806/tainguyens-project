Fully function web application from front end to back end):
	
-Application stimulates a simple dvd library on the web with basic function such as allow user to add dvd to the library and search a dvd by entering title, release date, director, studio, user rating and user note. (all the fields are incorporated with error checking).

-Used java as a core of back end, Spring, MySql as a database and front end being written by html,css and bootstrap.

-Applied authorization for different level of administration: user level can only see list of dvd in the library without capabilities to edit or delete any entry from the library. Administration level have full access to every functionality of the application from adding dvds, edit and delete from the library. (At up this now, authorization level is implement by using security tag on html file).

-JUnit tests were developed and successfully passed.



MySQL script to create the database:

*----------------------------------------
Create database called "dvd_library";

SET SQL_MODE= "NO_AUTO_VALUE_ON_ZERO";

SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `dvd_list`(

`dvd_id` int(10) NOT NULL AUTO_INCREMENT,
`title` varchar(50) NOT NULL,
`release_date` varchar(10) NOT NULL,
`director` varchar(50) NOT NULL,
`studio` varchar(50) NOT NULL,
`user_rating` varchar(50) NOT NULL,
`user_note` varchar(50) NOT NULL,
PRIMARY KEY (`dvd_id`)
) ENGINE =InnoDB DEFAULT CHARSET=latin1 auto_increment=1;

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL AUTO_INCREMENT,
`username` varchar(20) NOT NULL,
`password` varchar(20) NOT NULL,
`enabled` tinyint(1) NOT NULL,
PRIMARY KEY (`user_id`),
KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `users` (`user_id`, `username`, `password`, `enabled`) VALUES
(1, 'test', 'password', 1),
(2, 'test2', 'password', 1);

CREATE TABLE IF NOT EXISTS `authorities` (
`username` varchar(20) NOT NULL,
`authority` varchar(20) NOT NULL,
KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `authorities` (`username`, `authority`) VALUES
('test', 'ROLE_ADMIN'),
('test', 'ROLE_USER'),
('test2', 'ROLE_USER');

ALTER TABLE `authorities`
ADD CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES
`users` (`username`);

-------------------------------------------*
create a database called "dvd_library_test";

SET SQL_MODE= "NO_AUTO_VALUE_ON_ZERO";

SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `dvd_list_test`(

`dvd_id` int(10) NOT NULL AUTO_INCREMENT,
`title` varchar(50) NOT NULL,
`release_date` varchar(10) NOT NULL,
`director` varchar(50) NOT NULL,
`studio` varchar(50) NOT NULL,
`user_rating` varchar(50) NOT NULL,
`user_note` varchar(50) NOT NULL,
PRIMARY KEY (`dvd_id`)
) ENGINE =InnoDB DEFAULT CHARSET=latin1 auto_increment=1;

--------------------------------------------*
