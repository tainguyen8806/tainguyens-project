/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadDvds();
});



function clearDvdTable() {
    $('#contentRows').empty();
}

function loadDvds() {
    $.ajax({
        url: "dvds"
    }).success(function (data, status) {
        fillDvdTable(data, status);
    });
}
;

function fillDvdTable(tableList, status) {
    clearDvdTable();
    var dTable = $('#contentRows');

    $.each(tableList, function (index, dvd) {
        dTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.dvdId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(dvd.title)
                                )
                        )
                .append($('<td>').text(dvd.releaseDate))
                .append($('<td>').text(dvd.director))
                .append($('<td>').text(dvd.studio))
                );
    });
}
$("#detailsModal").on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.dvdId);
        modal.find('#dvd-title').text(dvd.title);
        modal.find('#dvd-release-date').text(dvd.releaseDate);
        modal.find('#dvd-director').text(dvd.director);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-rating').text(dvd.userRating);
        modal.find('#dvd-note').text(dvd.userNote);
    });
});