/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibmvn.Controller;

import com.mycompany.dvdlibmvn.dao.DvdLibDao;
import com.mycompany.dvdlibmvn.model.DVD;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
/**
 *
 * @author apprentice
 */
public class HomeController {

    private DvdLibDao dao;

    @Inject
    public HomeController(DvdLibDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home";
    }

    @RequestMapping(value = {"/mainAjaxPage"}, method = RequestMethod.GET)
    public String displayMainAjaxPage() {
        return "mainAjaxPage";
    }

    @RequestMapping(value = "/dvd", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public DVD createDvd(@Valid @RequestBody DVD dvd) {
// persist the incoming contact
        dao.addDVD(dvd);
// The addContact call to the dao assigned a contactId to the incoming
// Contact and set that value on the object. Now we return the updated
// object to the caller.
        return dvd;
    }

}
