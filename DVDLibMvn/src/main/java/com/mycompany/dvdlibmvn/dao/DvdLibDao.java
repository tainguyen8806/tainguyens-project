/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibmvn.dao;

import com.mycompany.dvdlibmvn.model.DVD;
import java.util.Map;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface DvdLibDao {

    public DVD addDVD(DVD dvd);
// remove the DVD with the given id from the data store

    public void removeDVD(int dvdId);
// update the given DVD in the data store

    public void updateDVD(DVD dvd);
// retrieve all DVDs from the data store

    public List<DVD> getAllDVDs();
// retrieve the DVD with the given id from the data store

    public DVD getDVDById(int dvdId);
// search for DVDs by the given search criteria values

    public List<DVD> searchDVDs(Map<SearchTerm, String> criteria);

}
