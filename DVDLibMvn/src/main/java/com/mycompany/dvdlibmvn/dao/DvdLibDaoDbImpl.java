/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibmvn.dao;

import com.mycompany.dvdlibmvn.model.DVD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DvdLibDaoDbImpl implements DvdLibDao {

    private final static String SQL_INSERT_DVD
            = "insert into dvd_list (title,release_date,director,studio,user_rating,user_note) values(?,?,?,?,?,?)";

    private final static String SQL_DELETE_DVD
            = "delete from dvd_list where dvd_id = ?";
    private final static String SQL_SELECT_DVD
            = "select * from dvd_list where dvd_id = ?";

    private final static String SQL_SELECT_ALL_DVDS
            = "select * from dvd_list";

    private final static String SQL_UPDATE_DVD
            = "update dvd_list set title = ?,release_date = ?,director = ?, studio = ?, user_rating = ?, user_note = ? where dvd_id = ?";

    private JdbcTemplate jdbcTemplate;

    public void setjdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final class DVDMapper implements ParameterizedRowMapper<DVD> {

        @Override
        public DVD mapRow(ResultSet rs, int i) throws SQLException {
            DVD dvd = new DVD();
            dvd.setDvdId(rs.getInt("dvd_id"));
            dvd.setTitle(rs.getString("title"));
            dvd.setReleaseDate(rs.getString("release_date"));
            dvd.setDirector(rs.getString("director"));
            dvd.setStudio(rs.getString("studio"));
            dvd.setUserRating(rs.getString("user_rating"));
            dvd.setUserNote(rs.getString("user_note"));
            return dvd;
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public DVD addDVD(DVD dvd) {
        jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getUserRating(),
                dvd.getUserNote());
        dvd.setDvdId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return dvd;
    }

    @Override
    public void removeDVD(int dvd_id) {
        jdbcTemplate.update(SQL_DELETE_DVD, dvd_id);
    }

    @Override
    public void updateDVD(DVD dvd) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getUserRating(),
                dvd.getUserNote(),
                dvd.getDvdId());
    }

    @Override
    public List<DVD> getAllDVDs() {
        return jdbcTemplate.query(SQL_SELECT_ALL_DVDS, new DVDMapper());
    }

    @Override
    public DVD getDVDById(int dvd_id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_DVD, new DVDMapper(), dvd_id);
        } catch (EmptyResultDataAccessException e) {
            return null;

        }
    }

    @Override
    public List<DVD> searchDVDs(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllDVDs();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from dvd_list where ");

            // build the where clause
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];

            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();
            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new DVDMapper(), paramVals);
        }

    }
}
