/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibmvn.dao;

import com.mycompany.dvdlibmvn.model.DVD;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDLibDaoInMemImpl implements DvdLibDao {

    private Map<Integer, DVD> DVDMap = new HashMap<>();
// used to assign ids to DVDs - simulates the auto increment
// primary key for DVDs in a database
    private static int DvdIdCounter = 0;

    @Override
    public DVD addDVD(DVD dvd) {
// assign the current counter values as the DvdId
        dvd.setDvdId(DvdIdCounter);
// increment the counter so it is ready for use for the next DVD
        DvdIdCounter++;
        DVDMap.put(dvd.getDvdId(), dvd);
        return dvd;
    }

    @Override
    public void removeDVD(int DVDId) {
        DVDMap.remove(DVDId);
    }

    @Override
    public void updateDVD(DVD DVD) {
        DVDMap.put(DVD.getDvdId(), DVD);
    }

    @Override
    public List<DVD> getAllDVDs() {
        Collection<DVD> c = DVDMap.values();
        return new ArrayList(c);
    }

    @Override
    public DVD getDVDById(int dvdId) {
        return DVDMap.get(dvdId);
    }

    @Override
    public List<DVD> searchDVDs(Map<SearchTerm, String> criteria) {
// Get all the search terms from the map
        String titleCriteria = criteria.get(SearchTerm.TITLE);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASE_DATE);
        String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);
        String userRatingCriteria = criteria.get(SearchTerm.USER_RATING);
        String userNoteCriteria = criteria.get(SearchTerm.USER_NOTE);
// Declare all the  predicate conditions
        Predicate< DVD> titleMatches;
        Predicate<DVD> releaseDateMatches;
        Predicate<DVD> directorMatches;
        Predicate< DVD> studioMatches;
        Predicate< DVD> userRatingMatches;
        Predicate< DVD> userNoteMatches;

        // Placeholder predicate - always returns true. Used for search terms
        // that are empty
        Predicate<DVD> truePredicate = (c) -> {
            return true;
        };
        // Assign values to predicates. If a given search term is empty, just
// assign the default truePredicate, otherwise assign the predicate that
// properly filters for the given term.
        titleMatches = (titleCriteria == null || titleCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getTitle().equals(titleCriteria);
        releaseDateMatches = (releaseDateCriteria == null || releaseDateCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getReleaseDate().equals(releaseDateCriteria);
        directorMatches = (directorCriteria == null || directorCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getDirector().equals(directorCriteria);
        studioMatches = (studioCriteria == null || studioCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getStudio().equals(studioCriteria);
        userRatingMatches = (userRatingCriteria == null || userRatingCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getUserRating().equals(userRatingCriteria);
        userNoteMatches = (userNoteCriteria == null || userNoteCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getUserNote().equals(userNoteCriteria);
// Return the list of DVDs that match the given criteria. To do this we
// just AND all the predicates together in a filter operation.
        return DVDMap.values().stream()
                .filter(titleMatches
                        .and(releaseDateMatches)
                        .and(directorMatches)
                        .and(studioMatches)
                        .and(userRatingMatches)
                        .and(userNoteMatches))
                .collect(Collectors.toList());
    }

}
