/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibmvn.dao;

import com.mycompany.dvdlibmvn.model.DVD;
import java.util.HashMap;
import java.util.List;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class DvdLibDaoTest {

    private DvdLibDao dao;

    public DvdLibDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (DvdLibDao) ctx.getBean("dvdLibDao");
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from dvd_list");

    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetDeleteDVD() {
// create new contact
        DVD nc = new DVD();
        nc.setTitle("Tear of the sun");
        nc.setReleaseDate("Doe");
        nc.setDirector("Oracle");
        nc.setStudio("john@doe.com");
        nc.setUserRating("1234445678");
        nc.setUserNote("I don't like this movie");
        dao.addDVD(nc);
        DVD fromDb = dao.getDVDById(nc.getDvdId());
        assertEquals(fromDb, nc);
        dao.removeDVD(nc.getDvdId());
        assertNull(dao.getDVDById(nc.getDvdId()));
    }

    @Test
    public void addUpdateDVD() {
        // create new contact
        DVD nc = new DVD();
        nc.setTitle("Lord of the Ring");
        nc.setReleaseDate("09-21-2008");
        nc.setDirector("tai nguyen");
        nc.setStudio("warnes bros");
        nc.setUserRating("PG13");
        nc.setUserNote("I just have to add this");
        dao.addDVD(nc);
        nc.setUserRating("9999999999");
        dao.updateDVD(nc);
        DVD fromDb = dao.getDVDById(nc.getDvdId());
        assertEquals(fromDb, nc);
    }

    @Test
    public void getAllDVDs() {
// create new contact
        DVD nc = new DVD();
        nc.setTitle("Lord of the Ring");
        nc.setReleaseDate("09-21-2008");
        nc.setDirector("tai nguyen");
        nc.setStudio("warnes bros");
        nc.setUserRating("PG13");
        nc.setUserNote("I just have to add this");
        dao.addDVD(nc);
// create new contact
        DVD nc2 = new DVD();
        nc2.setTitle("Tear of the sun");
        nc2.setReleaseDate("09-22-2009");
        nc2.setDirector("Bruce Willis");
        nc2.setStudio("MGM");
        nc2.setUserRating("R");
        nc2.setUserNote("I just have to add this");
        dao.addDVD(nc2);
        List<DVD> cList = dao.getAllDVDs();
        assertEquals(cList.size(), 2);
    }

    @Test
    public void searchDVDs() {
// create new contact
        DVD nc = new DVD();
        nc.setTitle("Lord of the Ring");
        nc.setReleaseDate("09-21-2008");
        nc.setDirector("tai nguyen");
        nc.setStudio("warnes bros");
        nc.setUserRating("PG13");
        nc.setUserNote("I just have to add this");
        dao.addDVD(nc);
// create new contact
        DVD nc2 = new DVD();
        nc2.setTitle("Tear of the sun");
        nc2.setReleaseDate("09-22-2009");
        nc2.setDirector("Bruce Willis");
        nc2.setStudio("MGM");
        nc2.setUserRating("R");
        nc2.setUserNote("I just have to add this");
        dao.addDVD(nc2);
// create new contact - same last name as first contact but different
// company
        DVD nc3 = new DVD();
        nc3.setTitle("Ted");
        nc3.setReleaseDate("09-21-2008");
        nc3.setDirector("Blue");
        nc3.setStudio("Disney");
        nc3.setUserRating("Teen");
        nc3.setUserNote("I just have to add this");
        dao.addDVD(nc3);
// Create search criteria
        Map<SearchTerm, String> criteria = new HashMap<>();
        criteria.put(SearchTerm.RELEASE_DATE, "09-22-2009");
        List<DVD> cList = dao.searchDVDs(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc2, cList.get(0));
        // New search criteria - look for 09-21-2008
        criteria.put(SearchTerm.RELEASE_DATE, "09-21-2008");
        cList = dao.searchDVDs(criteria);
        assertEquals(2, cList.size());
// Add company to search criteria
        criteria.put(SearchTerm.DIRECTOR, "tai nguyen");
        cList = dao.searchDVDs(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc, cList.get(0));
// Change company to Blue, should get back nc3
        criteria.put(SearchTerm.DIRECTOR, "Blue");
        cList = dao.searchDVDs(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc3, cList.get(0));
// Change company to Bruce Willis, should get back nothing
        criteria.put(SearchTerm.DIRECTOR, "Bruce Willis");
        cList = dao.searchDVDs(criteria);
        assertEquals(0, cList.size());
    }

}
