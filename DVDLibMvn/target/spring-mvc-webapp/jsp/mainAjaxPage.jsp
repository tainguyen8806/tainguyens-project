<%-- 
    Document   : home
    Created on : Oct 16, 2015, 1:21:18 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                    <a href="${pageContext.request.contextPath}/mainAjaxPage">Add new DVD
                    </a>
                    </li>

                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/list">List All DVDs</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/j_spring_security_logout">LogOut</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <h2 class="col-md-offset-4">Add new DVD</h2><br>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="add-title" class="col-md-4 control-label">
                        Title:
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-title" placeholder="Title"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-release-date" class="col-md-4 control-label">
                        Release Date:
                    </label>
                    <div class="col-md-8">
                        <input type="date" class="form-control" id="add-release-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-director" class="col-md-4 control-label">
                        Director:
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-director" placeholder="director"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-studio" class="col-md-4 control-label">
                        Studio:
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-studio" placeholder="studio"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-rating" class="col-md-4 control-label">
                        User rating:
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-rating" placeholder="Add rating"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for ="add-note" class="col-md-4 control-label">
                        User note:
                    </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-note" placeholder="Add note"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-default">
                            Create DVD
                        </button>
                    </div>
                </div>
            </form>
            <div id="validationErrors" style="color:red"></div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/newList.js"></script>
    </body>
</html>
