<%-- 
    Document   : search
    Created on : Oct 16, 2015, 1:41:06 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/mainAjaxPage">Add new DVD
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/list">List All DVDs</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/j_spring_security_logout">LogOut</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"><h2 class="col-md-offset-4">List DVDs</h2>
                <table id="dvdTable" class="table table-hover col-md-offset-1">
                    <tr>
                        <th width="20%">Title</th>
                        <th width="20%">Release Date</th>
                        <th width="20%">Director</th>
                        <th width="10%">Studio</th>
                        <th width="10%"></th>
                        <th width="10%"></th>
                    </tr>
                    <tbody id="contentRows"></tbody>
                </table>
            </div>
            <div class="col-md-5 col-md-offset-1">
                <h2 class="col-md-offset-2">Search DVD</h2><br>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="add-title" class="col-md-2 control-label">
                            Title:
                        </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="search-title" placeholder="Title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for ="add-release-date" class="col-md-2 control-label">
                            Release Date:
                        </label>
                        <div class="col-md-6">
                            <input type="date" class="form-control" id="search-release-date"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for ="add-director" class="col-md-2 control-label">
                            Director:
                        </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="search-director" placeholder="director"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for ="add-studio" class="col-md-2 control-label">
                            Studio:
                        </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="search-studio" placeholder="studio"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for ="add-rating" class="col-md-2 control-label">
                            User rating:
                        </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="search-rating" placeholder="Add rating"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for ="add-note" class="col-md-2 control-label">
                            User note:
                        </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="search-note" placeholder="Add note"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-6">
                            <button type="submit" id="search-button" class="btn btn-default">
                                Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="detailsModalLabel">Edit DVD</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-title" class="col-md-4 control-label">
                                    Title:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-title"
                                           placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-release-date" class="col-md-4 control-label">
                                    Release Date:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-release-date"
                                           placeholder="release date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-director" class="col-md-4 control-label">Director:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-director"
                                           placeholder="Director">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio" class="col-md-4 control-label">
                                    Studio:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-studio"
                                           placeholder="Studio">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-rating" class="col-md-4 control-label">
                                    User Rating:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-rating" placeholder="User Rating">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-note" class="col-md-4 control-label">
                                    User Note:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-note" placeholder="User Note">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" id="edit-button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Edit DVD
                                    </button>
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Cancel
                                    </button>
                                    <input type="hidden" id="edit-dvd-id">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span> </button>
                        <h4 class="modal-title" id="detailsModalLabel">Contact Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Title:</th>
                                <td id="dvd-title"></td>
                            </tr> <tr>
                                <th>Release Date:</th>
                                <td id="dvd-release-date"></td> </tr>
                            <tr>
                                <th>Director:</th>
                                <td id="dvd-director"></td>
                            </tr> <tr>
                                <th>Studio:</th>
                                <td id="dvd-studio"></td> </tr>
                            <tr> <th>User Rating:</th>
                                <td id="dvd-rating"></td> </tr>
                            <tr> <th>User Note:</th>
                                <td id="dvd-note"></td> </tr> 
                        </table> </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button> </div>
                </div> 
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <sec:authorize access="hasRole('ROLE_USER')">
        <script src="${pageContext.request.contextPath}/js/user.js"></script>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <script src="${pageContext.request.contextPath}/js/newList.js"></script>
    </sec:authorize>
</body>
</html>
