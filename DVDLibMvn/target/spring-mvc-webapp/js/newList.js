/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    loadDvds();
});
$('#add-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'dvd',
        data: JSON.stringify({
            title: $('#add-title').val(),
            releaseDate: $('#add-release-date').val(),
            director: $('#add-director').val(),
            studio: $('#add-studio').val(),
            userRating: $('#add-rating').val(),
            userNote: $('#add-note').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#add-title').val('');
        $('#add-release-date').val('');
        $('#add-director').val('');
        $('#add-studio').val('');
        $('#add-rating').val('');
        $('#add-note').val('');
        $('#validationErrors').empty();
        loadDvds();
    }).error(function (data, status) {
        $.each(data.responseJSON.fieldErrors, function (index, validationError) {
            var errorDiv = $('#validationErrors');
            errorDiv.append(validationError.message).append($('<br>'));
        });
    });
});

function loadDvds() {
    $.ajax({
        url: "dvds"
    }).success(function (data, status) {
        fillDvdTable(data, status);
    });
}
;

function fillDvdTable(tableList, status) {
    clearDvdTable();
    var dTable = $('#contentRows');

    $.each(tableList, function (index, dvd) {
        dTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.dvdId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(dvd.title)
                                )
                        )
                .append($('<td>').text(dvd.releaseDate))
                .append($('<td>').text(dvd.director))
                .append($('<td>').text(dvd.studio))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.dvdId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')
                                )
                        )
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteDVD(' + dvd.dvdId + ')'
                                }).text('Delete')
                                )
                        )

                );
    });
}


$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.dvdId);
        modal.find('#edit-dvd-id').val(dvd.dvdId);
        modal.find('#edit-title').val(dvd.title);
        modal.find('#edit-release-date').val(dvd.releaseDate);
        modal.find('#edit-director').val(dvd.director);
        modal.find('#edit-studio').val(dvd.studio);
        modal.find('#edit-rating').val(dvd.userRating);
        modal.find('#edit-note').val(dvd.userNote);
    });
});

$("#detailsModal").on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.dvdId);
        modal.find('#dvd-title').text(dvd.title);
        modal.find('#dvd-release-date').text(dvd.releaseDate);
        modal.find('#dvd-director').text(dvd.director);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-rating').text(dvd.userRating);
        modal.find('#dvd-note').text(dvd.userNote);
    });
});

$('#search-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/dvds',
        data: JSON.stringify({
            title: $('#search-title').val(),
            releaseDate: $('#search-release-date').val(),
            director: $('#search-director').val(),
            studio: $('#search-studio').val(),
            userRating: $('#search-rating').val(),
            userNote: $('#search-note').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#search-title').val('');
        $('#search-release-date').val('');
        $('#search-director').val('');
        $('#search-studio').val('');
        $('#search-rating').val('');
        $('#search-note').val('');
        fillDvdTable(data, status);
    });
});

function clearDvdTable() {
    $('#contentRows').empty();
}
$('#edit-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'PUT',
        url: 'dvd/' + $('#edit-dvd-id').val(),
        data: JSON.stringify({
            dvdId: $('#edit-dvd-id').val(),
            title: $('#edit-title').val(),
            releaseDate: $('#edit-release-date').val(),
            director: $('#edit-director').val(),
            studio: $('#edit-studio').val(),
            userRating: $('#edit-rating').val(),
            userNote: $('#edit-note').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function () {
        loadDvds();
    });
});
function deleteDVD(id) {
    var answer = confirm("Do you really want to delete this DVD?");
    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            loadDvds();
        });
    }
}



