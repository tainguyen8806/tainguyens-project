/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Gon
 */
@Component
public class CDPlayer implements MediaPlayer {
    private CompactDisc compactDisc;
    
    @Autowired(required=false)
    public CDPlayer(CompactDisc compactDisc){
        this.compactDisc = compactDisc;
    }
    
    
    public void play(){
        compactDisc.play();
    }
    
    
    
}
