Fully function web application from front end to back end:

-Application stimulate a simple blog application : allow user to create post, edit, delete, adding tag for searching purposes, adding and edit user.

- Admin has full access to all the functionality of the application:
	- Add,approve, edit and delete post.
	- Add, edit and delete user.
- User(marketing manager) can log in and create post for the blog but post created by user will have a pending status which means the post will not be shown in the blog page without being approved by admin.

- Visitor can only see the blog page.

- A post has few features:
	- Can be associated with tags for searching purposes.
	- Can be set with published date and expired date.
	(default value for published date would be current date and Dec 31,3000 for expired date).

( This project is a group project of Tai Nguyen, Chet Kuhn and Asley Sinner)


MySQL script to create the database:

*--------------------------------------------
Create a database called "Blog";

Set SQL_MODE= "NO_AUTO_VALUE_ON_ZERO";
set time_zone = "+00:00";
create table if not exists `post`(
`post_id` int(11) not null auto_increment,
`title` varchar(50) not null,
`content` text not null,
`status` varchar(1) not null,
`date` date not null,
`expired_date` date not null,
primary key (`post_id`)
) Engine =InnoDB default charset=latin1 auto_increment=1;
create table if not exists `tag`(
`tag_id` int(11) not null auto_increment,
`tag_name` varchar(50) not null,
primary key (`tag_id`)
) Engine = InnoDB default charset=latin1 auto_increment = 1;
create table if not exists `post_tag`(
`post_id` int(11) not null,
`tag_id` int(11) not null,
key `post_id`(`post_id`),
key `tag_id`(`tag_id`)
) Engine= InnoDB default charset=latin1;

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL AUTO_INCREMENT,
`username` varchar(20) NOT NULL,
`password` varchar(20) NOT NULL,
`enabled` tinyint(1) NOT NULL,
PRIMARY KEY (`user_id`),
KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `users` (`user_id`, `username`, `password`, `enabled`) VALUES
(1, 'test', 'password', 1),
(2, 'test2', 'password', 1);

CREATE TABLE IF NOT EXISTS `authorities` (
`username` varchar(20) NOT NULL,
`authority` varchar(20) NOT NULL,
KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `authorities` (`username`, `authority`) VALUES
('test', 'ROLE_ADMIN'),
('test', 'ROLE_USER'),
('test2', 'ROLE_USER');

ALTER TABLE `authorities`
ADD CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES
`users` (`username`);

------------------------------------------------------------------------------*

Create a database called "Blog_Test";

Set SQL_MODE= "NO_AUTO_VALUE_ON_ZERO";
set time_zone = "+00:00";
create table if not exists `post`(
`post_id` int(11) not null auto_increment,
`title` varchar(50) not null,
`content` text not null,
`status` varchar(1) not null,
`date` date not null,
`expired_date` date not null,
primary key (`post_id`)
) Engine =InnoDB default charset=latin1 auto_increment=1;
create table if not exists `tag`(
`tag_id` int(11) not null auto_increment,
`tag_name` varchar(50) not null,
primary key (`tag_id`)
) Engine = InnoDB default charset=latin1 auto_increment = 1;
create table if not exists `post_tag`(
`post_id` int(11) not null,
`tag_id` int(11) not null,
key `post_id`(`post_id`),
key `tag_id`(`tag_id`)
) Engine= InnoDB default charset=latin1;

*-----------------------------------------------------------------------------