/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    fillStatic();
    loadAllUsers();

});

theuserid = 0;
theusername = "";
thesecname = "";


function fillStatic() {
    $.ajax({
        url: "staticcontent",
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    }).success(function (data, status) {
        $("#news").append(data.content);
    });

}

function loadAllUsers() {
// Make an Ajax GET call to the 'allpost' endpoint. Iterate through
// each of the JSON objects that are returned and render them to the
// summary table
    $.ajax({
        url: "alluser"
    }).success(function (data, status) {
        fillUserTable(data, status);

    }).error(function () {
        alert("Error on loadAllUsers");
    });
}
;

function fillUserTable(userList, status) {
    $('#usertable').empty();
    var uTable = $('#usertable');
// render the new post data to the table
    $.each(userList, function (index, user) {

        var p_content = "Username: " + user.name;

//create Delete element
        var d_id = "d" + user.userid;
        var d_element = document.createElement("button");
        var d_function = "removeUser(" + user.userid + ")";
        d_element.setAttribute("id", d_id);
        d_element.setAttribute("class", "button btn btn-info btn-lg");
        d_element.setAttribute("onclick", d_function);
        var d_text = document.createTextNode("Delete");
        d_element.appendChild(d_text);

//create Edit element
        var e_id = "e" + user.userid;
        var e_function = "editUser(" + user.userid + ")";
        var e_element = document.createElement("button");
        e_element.setAttribute("id", e_id);
        e_element.setAttribute("class", "button btn btn-info btn-lg");
        e_element.setAttribute("onclick", e_function);
        e_element.setAttribute("data-toggle", "modal");
        e_element.setAttribute("data-target", "#manageModal");
        var e_text = document.createTextNode("Edit");
        e_element.appendChild(e_text);

        uTable.append($("<tr>")
                .append(p_content)
                )
                .append($("<div class='buttonrow' style='float: right'>")
                        .append(e_element)
                        .append(d_element)
                        )
                .append("<hr>");
    });
    //create Add Button element

    var a_element = document.createElement("button");
    a_element.setAttribute("class", "button");
    a_element.setAttribute("data-toggle", "modal");
    a_element.setAttribute("data-target", "#addModal");
    var a_text = document.createTextNode("Add a supervisor");
    a_element.appendChild(a_text);
    uTable.append($("<tr>")
            .append(a_element)
            );

}

function editUser(user_id) {
    $.ajax({
        type: 'GET',
        url: "getuser/" + user_id
    }).success(function (user, status) {
        theuserid = user.userid;
        theusername = user.name;
        thesecname = user.password;
        fillEditModal();
    }).error(function () {
        alert("editPost error");
    });
}
;

function removeUser(user_id) {
    if (confirm("Are you sure you want to delete this User? Press OK to continue"))
    {
        $.ajax({
            type: 'DELETE',
            url: "alluser/" + user_id
        }).success(function (user, status) {
            loadAllUsers();
            fillUserTable();
        }).error(function () {
            alert("removeUser error");
        });
    }
}
;


function fillEditModal() {

    
    var elementone = document.getElementById('fieldone');
    elementone.innerHTML="";
    var f_one = document.createTextNode("Username: " + theusername);
    var elementtwo = document.getElementById('fieldtwo');
    elementtwo.setAttribute("value", thesecname);
    elementone.appendChild(f_one);
}
;

$('#add-user').click(function (event) {
    $.ajax({
        type: 'POST',
        url: 'alluser/',
        data: JSON.stringify({
            name: $('#add_username').val(),
            password: $('#add_pass').val()
        }),
        headers: {
            'Accept': 'application/json', 'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function () {
        $('#addModal').modal('hide');
        loadAllUsers();
        fillUserTable();
    }).error(function (data, status) {
        alert("modal error!");
    });
});

$('#edit-user').click(function (event) {
    var f_two = $('#fieldtwo').val();
    var f_three = $('#fieldthree').val();
    if (f_two === f_three) {
        $.ajax({
            type: 'PUT',
            url: 'alluser/' + theuserid,
            data: JSON.stringify({
                userid: theuserid,
                name: theusername,
                password: $('#fieldtwo').val()
            }),
            headers: {
                'Accept': 'application/json', 'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function () {
            $('#manageModal').modal('hide');
            loadAllUsers();
            fillUserTable();
        }).error(function (data, status) {
            alert("modal error!");
        });
    } else {
        alert("Passwords do not match!");
    }
});

//Footer unnamed function
if ($(".Page").height() < $(window).height()) {
    $(".footer").addClass("fixed");
} else {
    $(".footer").removeClass("fixed");
}
;
$(window).bind("load", function () {

    var footerHeight = 0,
            footerTop = 0,
            $footer = $("#footer");
    positionFooter();
    function positionFooter() {

        footerHeight = $footer.height();
        footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";
        if (($(document.body).height() + footerHeight) < $(window).height()) {
            $footer.css({
                position: "absolute"
            }).animate({
                top: footerTop
            });
        } else {
            $footer.css({
                position: "static"
            });
        }

    }

    $(window)
            .scroll(positionFooter)
            .resize(positionFooter);
});


