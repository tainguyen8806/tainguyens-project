/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global tinyMCE */

$(document).ready(function () {
    fillStatic();
    loadAllPosts();
    fillTag();
});

var staticcontent = "";
var thepostid = 0;
var thepostcontent = "";
var theposttitle = "";

function fillStatic() {
    $.ajax({
        url: "staticcontent",
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    }).success(function (data, status) {
        $("#news").empty();
        var e_function = "editStatic()";
        var e_element = document.createElement("button");
        e_element.setAttribute("class", "button btn btn-info btn-lg");
        e_element.setAttribute("onclick", e_function);
        e_element.setAttribute("data-toggle", "modal");
        e_element.setAttribute("data-target", "#staticModal");
        e_element.setAttribute("style", "float: right");
        var e_text = document.createTextNode("Edit");
        e_element.appendChild(e_text);
        $("#news").append(data.content)
                .append("<br>")
                .append(e_element);

    });

}


if ($(".Page").height() < $(window).height()) {
    $(".footer").addClass("fixed");
} else {
    $(".footer").removeClass("fixed");
}
;
$(window).bind("load", function () {

    var footerHeight = 0,
            footerTop = 0,
            $footer = $("#footer");
    positionFooter();
    function positionFooter() {

        footerHeight = $footer.height();
        footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";
        if (($(document.body).height() + footerHeight) < $(window).height()) {
            $footer.css({
                position: "absolute"
            }).animate({
                top: footerTop
            });
        } else {
            $footer.css({
                position: "static"
            });
        }

    }

    $(window)
            .scroll(positionFooter)
            .resize(positionFooter);
});


function editPost(post_id) {
    $.ajax({
        type: 'GET',
        url: "getpost/" + post_id
    }).success(function (post, status) {
        thepostid = post.postId;
        theposttitle = post.title;
        thepostcontent = post.blogContent;
        fillEditModal(post_id);
    }).error(function () {
        alert("editPost error");
    });
}
;

function editStatic() {
    $.ajax({
        url: "staticcontent"
    }).success(function (data, status) {
        staticcontent = data.content;
        fillStaticModal();
    }).error(function () {
        alert("editStatic error");
    });
}
;

function deletePost(post_id) {
   if (confirm("Are you sure you want to delete this post? Press OK to continue"))
   {        $.ajax({
           type: 'GET',
           url: "getpost/" + post_id
       }).success(function (post) {
           $.ajax({
               type: 'PUT',
               url: "deletepost/" + post_id,
               data: JSON.stringify({
                   postId: post.postId,
                   title: post.title,
                   blogContent: post.blogContent,
                   date: post.date,
                   expiredDate: post.expiredDate,
                   status: "d"
               }),
               headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'
               },
               'dataType': 'json'
           }).success(function () {
               loadAllPosts();
               alert("You have been successfully delete post " + post.postId);
           });
//    });
//
//        alert("Post not really deleted, but the function works");
       });
   }
   else {
       alert("Failed.");
   }
}




function fillStaticModal() {
    tinymce.init({
        selector: "textarea#static-content",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
    tinyMCE.activeEditor.setContent(staticcontent);
}
;



function fillEditModal(post_id) {
    var element = document.getElementById('title');
    element.setAttribute("value", theposttitle);
    tinymce.init({
        selector: "textarea#blog-content",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
    tinyMCE.activeEditor.setContent(thepostcontent);
}
;

function loadAllPosts() {
// Make an Ajax GET call to the 'allpost' endpoint. Iterate through
// each of the JSON objects that are returned and render them to the
// summary table
    $.ajax({
        url: "allpost"
    }).success(function (data, status) {
        fillBlog(data, status);
        var path = window.location.pathname;
        var page = path.split("/").pop();
        if ((page === "home") || (page === "")) {
            $(".buttonrow").empty();
        }
        ;
    }).error(function () {
        alert("Handler for .error() called.");
    });
}
;


function fillBlog(postList, status) {
// clear the previous list
    clearBlog();
// grab the tbody element that will hold the new list of posts.
    var cTable = $('#blogroll');
// render the new post data to the table
    $.each(postList, function (index, post) {

// build a title element
        var t_element = document.createElement("h3");
        var t_content = post.title;
        t_element.innerHTML = t_content;
//  build a delete button element
        var d_id = "d" + post.postId;
        var d_element = document.createElement("button");
        var d_function = "deletePost(" + post.postId + ")";
        d_element.setAttribute("id", d_id);
        d_element.setAttribute("class", "button btn btn-info btn-lg");
        d_element.setAttribute("onclick", d_function);
        var d_text = document.createTextNode("Delete");
        d_element.appendChild(d_text);
//  build an edit button element
        var e_id = "e" + post.postId;
        var e_function = "editPost(" + post.postId + ")";
        var e_element = document.createElement("button");
        e_element.setAttribute("id", d_id);
        e_element.setAttribute("class", "button btn btn-info btn-lg");
        e_element.setAttribute("onclick", e_function);
        e_element.setAttribute("data-toggle", "modal");
        e_element.setAttribute("data-target", "#myModal");
        var e_text = document.createTextNode("Edit");
        e_element.appendChild(e_text);
//  build a <p> element to hold the post content
        var p_name = "p_name" + post.postId;
        var p_content = post.blogContent;
        var p_element = document.createElement("p");
        p_element.setAttribute("id", p_name);
        p_element.innerHTML = p_content;
//  construct the table of blog posts
        cTable.append(t_element)
                .append("<br>")

                .append($("<tr>")
                        .append(p_element)
                        )
                .append($("<div class='buttonrow' style='float: right'>")
                        .append(e_element)
                        .append(d_element)
                        )
                .append("<hr>");
    });
}
;
function clearBlog() {
    $('#blogroll').empty();
}
;



$('#edit-post').click(function (event) {
    $.ajax({
        type: 'PUT',
        url: 'post/' + thepostid,
        data: JSON.stringify({
            postId: thepostid,
            title: $('#title').val(),
            blogContent: tinyMCE.activeEditor.getContent({format: 'raw'})
        }),
        headers: {
            'Accept': 'application/json', 'Content-Type': 'application/json'
        },
        'dataType': 'json'
    })
            .success(function () {
                loadAllPosts();
                $('#myModal').modal('hide');
                location.reload();
            }).error(function (data, status) {
        alert("modal error!");
    });
});

$('#edit-static').click(function (event) {
    $.ajax({
        type: 'PUT',
        url: 'staticcontent/',
        data: JSON.stringify({
            id: 1,
            content: tinyMCE.activeEditor.getContent({format: 'raw'})
        }),
        headers: {
            'Accept': 'application/json', 'Content-Type': 'application/json'
        },
        'dataType': 'json'
    })
            .success(function () {
                fillStatic();
                $('#staticModal').modal('hide');
//                location.reload();
            }).error(function (data, status) {
        alert("modal error!");
    });
});



function fillTag() {
    var tagTable = $('#tagroll');
    $.ajax({
        url: 'alltags'
    }).success(function (taglist) {
//        alert("this is working");
        console.log(taglist.length);
        for (var i = 0; i < taglist.length; i += 3) {
            tagTable.append($('<tr>')
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'tagSearch(' + taglist[i].tagId + ')'
                                    })
                                    .text(taglist[i].tagName)
                                    )
                            )
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'tagSearch(' + taglist[i + 1].tagId + ')'
                                    })
                                    .text(taglist[i + 1].tagName)
                                    )
                            )
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'tagSearch(' + taglist[i + 2].tagId + ')'
                                    })
                                    .text(taglist[i + 2].tagName)
                                    )
                            )

                    );
        }
    }).error(function () {
        alert("this is not working");
    });
}
;
function tagSearch(id) {
    alert("this is function right now");
    $.ajax({
        type: 'GET',
        url: "post/" + id
    }).success(function (data, status) {
        fillBlog(data, status);
    });
}
;
