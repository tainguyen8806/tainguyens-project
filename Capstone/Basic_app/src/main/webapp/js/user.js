/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global tinyMCE */

$(document).ready(function () {
    loadAllPosts();
    fillTag();
});

if ($(".Page").height() < $(window).height()) {
    $(".footer").addClass("fixed");
} else {
    $(".footer").removeClass("fixed");
}

$(window).bind("load", function () {

    var footerHeight = 0,
            footerTop = 0,
            $footer = $("#footer");

    positionFooter();

    function positionFooter() {

        footerHeight = $footer.height();
        footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";

        if (($(document.body).height() + footerHeight) < $(window).height()) {
            $footer.css({
                position: "absolute"
            }).animate({
                top: footerTop
            });
        } else {
            $footer.css({
                position: "static"
            });
        }

    }

    $(window)
            .scroll(positionFooter)
            .resize(positionFooter);

});

function loadAllPosts() {
// Make an Ajax GET call to the 'allpost' endpoint. Iterate through
// each of the JSON objects that are returned and render them to the
// summary table
    $.ajax({
        url: "allpost"
    }).success(function (data, status) {
        fillBlog(data, status);
        var path = window.location.pathname;
        var page = path.split("/").pop();
        if ((page === "home") || (page === "")) {
            console.log(page);
            $(".buttonrow").empty();
        }
        ;
    }).error(function () {
        alert("Handler for .error() called.");
    });
}
;

function fillBlog(postList, status) {
// clear the previous list
    clearBlog();
// grab the tbody element that will hold the new list of posts.
    var cTable = $('#blogroll');
// render the new post data to the table
    $.each(postList, function (index, post) {
        var telement = document.createElement("h3");
        var tcontent = post.title;
        telement.innerHTML = tcontent;
        var pname = "pname" + post.postId;
        var pcontent = post.blogContent;
        var pelement = document.createElement("p");
        pelement.setAttribute("id", pname);
        pelement.innerHTML = pcontent;
        cTable.append(telement)
                .append("<br>")

                .append($("<tr>")
                        .append(pelement)
                        )
                .append("<hr>");
    });
}
;


// Clear all content rows from the summary table
function clearBlog() {
    $('#blogroll').empty();
}

function fillTag() {


    var tagTable = $('#tagroll');
    $.ajax({
        url: 'alltags'
    }).success(function (taglist) {
        console.log(taglist.length);
        for (var i = 0; i < taglist.length; i += 3) {
            tagTable.append($('<tr>')
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'tagSearch(' + taglist[i].tagId + ')'
                                    })
                                    .text(taglist[i].tagName)
                                    )
                            )
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'tagSearch(' + taglist[i + 1].tagId + ')'
                                    })
                                    .text(taglist[i + 1].tagName)
                                    )
                            )
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'tagSearch(' + taglist[i + 2].tagId + ')'
                                    })
                                    .text(taglist[i + 2].tagName)
                                    )
                            )

                    );
        }
    }).error(function () {
        alert("this is not working");
    });
}
;

function tagSearch(id) {
    $.ajax({
        type: 'GET',
        url: "post/" + id
    }).success(function (data, status) {
        fillBlog(data, status);
    });

}


