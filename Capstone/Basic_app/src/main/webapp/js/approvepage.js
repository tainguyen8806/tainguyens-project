

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global tinyMCE */

$(document).ready(function () {
    loadAllPosts();
});

if ($(".Page").height() < $(window).height()) {
    $(".footer").addClass("fixed");
} else {
    $(".footer").removeClass("fixed");
}

$(window).bind("load", function () {

    var footerHeight = 0,
            footerTop = 0,
            $footer = $("#footer");

    positionFooter();

    function positionFooter() {

        footerHeight = $footer.height();
        footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";

        if (($(document.body).height() + footerHeight) < $(window).height()) {
            $footer.css({
                position: "absolute"
            }).animate({
                top: footerTop
            });
        } else {
            $footer.css({
                position: "static"
            });
        }

    }

    $(window)
            .scroll(positionFooter)
            .resize(positionFooter);

});

function loadAllPosts() {
// Make an Ajax GET call to the 'allpost' endpoint. Iterate through
// each of the JSON objects that are returned and render them to the
// summary table
    $.ajax({
        url: "approveposts"
    }).success(function (data, status) {
        fillBlog(data, status);
    }).error(function () {
        alert("Handler for .error() called.");
    });
}
;

function fillBlog(postList, status) {
// clear the previous list
    clearBlog();
// grab the tbody element that will hold the new list of posts.
    var cTable = $('#approve-post');
// render the new post data to the table
    $.each(postList, function (index, post) {
        var telement = document.createElement("h3");
        var tcontent = post.title;
        telement.innerHTML = tcontent;
        var pname = "pname" + post.postId;
        var pcontent = post.blogContent;
        var pelement = document.createElement("p");
        pelement.setAttribute("id", pname);
        pelement.innerHTML = pcontent;
        cTable.append(telement)
                .append("<br>")

                .append($("<tr>")
                        .append(pelement)
                        )
                .append($("<div class='buttonrow'>")
                        .append($("<a class='button'>")
                                .attr({
                                    'onClick': 'approvePost(' + post.postId + ')'
                                })
                                .text("Approve")
                                )

                        .append($("<a class='button'>")
                        .attr({
                            'onClick': 'deletePost(' + post.postId +')'
                                })
                                .text("Delete")
                                )
                        )
                .append("<hr>");
    });
}
;


// Clear all content rows from the summary table
function clearBlog() {
    $('#approve-post').empty();
}

function approvePost(postId) {

    $.ajax({
        type: 'GET',
        url: 'approvepost/' + postId
    }).success(function (post) {
        $.ajax({
            type: 'PUT',
            url: 'approvepost/' + postId,
            data: JSON.stringify({
                postId: post.postId,
                title: post.title,
                blogContent: post.blogContent,
                date: post.date,
                expiredDate: post.expiredDate,
                status: "a"
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function () {
            loadAllPosts();
            alert("You have been successfully approve post " + post.postId);
        });
    });
}

function deletePost(post_id) {
   if (confirm("Are you sure you want to delete this post? Press OK to continue"))
   {        $.ajax({
           type: 'GET',
           url: "getpost/" + post_id
       }).success(function (post) {
           $.ajax({
               type: 'PUT',
               url: "deletepost/" + post_id,
               data: JSON.stringify({
                   postId: post.postId,
                   title: post.title,
                   blogContent: post.blogContent,
                   date: post.date,
                   expiredDate: post.expiredDate,
                   status: "d"
               }),
               headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'
               },
               'dataType': 'json'
           }).success(function () {
               loadAllPosts();
               alert("You have been successfully delete post " + post.postId);
           });
//    });
//
//        alert("Post not really deleted, but the function works");
       });
   }
   else {
       alert("Failed.");
   }
}
