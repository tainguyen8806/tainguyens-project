<%-- 
    Document   : approve
    Created on : Nov 9, 2015, 5:35:48 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dispensation|Drones</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/personalizedcss.css" rel="stylesheet"/>
        <!-- dis Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/drone.jpg">
    </head>
    <body>
        <div class="container">

            <h1>Dispensation</h1>

            <div class="col-md-2 col-sm-offset-2 row top-buffer-2">
            </div>
            <div class="container">
                <form method="post" action="j_spring_security_check" class="signin form-inline col-md-2 pull-right" role="form">
                    <input type="button" onclick="location.href = '${pageContext.request.contextPath}/j_spring_security_logout'" value="Log Out" />
                </form>
                <c:if test="${param.login_error == 1}">
                    <h3>Wrong id or password!</h3>
                </c:if>

            </div>
            <div class="navbar">
                <ul id="nav">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/securehome">Home</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/addpost">Add Post</a></li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/approve">Approve</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/manageusers">Manage User</a></li>
                </ul>
            </div>

            <div class ="row">
                <div class ="col-md-8">
                    <h2></h2>
                    <table class="table table-hover">
                        <tbody id="approve-post">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="footer-dif">
            <p class="text-center">©Copyright 2015 Tai, Chet, and Ashley</p>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/approvepage.js"></script>
    </body>
</html>
