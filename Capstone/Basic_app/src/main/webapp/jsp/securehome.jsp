<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dispensation|Drones</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/personalizedcss.css" rel="stylesheet"/>
        <!-- dis Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/drone.jpg">
    </head>
    <body>
         <img src="${pageContext.request.contextPath}/img/extreme-drone.jpg" id="background" width="100%" height="100%">
        <div class="container">

            <h1>Dispensation</h1>

     
            <div class="container">
                <form method="post" action="j_spring_security_check" class="signin form-inline col-md-2 pull-right" role="form">
                    <input type="button" onclick="location.href = '${pageContext.request.contextPath}/j_spring_security_logout'" value="Log Out" />
                </form>
            </div>
            <div class="navbar">
                <ul id="nav">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/securehome">Home</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/addpost">Add Post</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/approve">Approve</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/manageusers">Manage User</a></li>
                    
                </ul>
            </div>

            <div class ="row">
                <div class ="col-md-8">
                    <h2></h2>
                    <table class="table table-hover">
                        <tbody id="blogroll">
                        </tbody>
                    </table>

                </div>
                <div id="sidebar">
                    <p id='news' class='staticnews'></p>
                    <h3>Tags</h3>
                    <table>
                        <tr>
                            <th width="50%"></th>
                            <th width="50%"></th>
                        </tr>
                        <tbody id="tagroll">

                        </tbody>
                    </table>
                    <p class="news"> Dispensation <br> 601 Nicollet Mall <br> Minneapolis, MN 55402 </p>
                </div>
            </div>
        </div>
        <!-- Modal Edit post-->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Post</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <p id="modal_id"></p>
                            <p>Title</p>
                            <input type="text" id="title" style="width: 100%" />
                            <h4>Content</h4>
                            <textarea id="blog-content" style="width:100%">
                            </textarea>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type ="submit" id="edit-post" class ="btn btn-default">
                            Submit
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal Edit Static -->
        <div id="staticModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Post</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <h4>Content</h4>
                            <textarea id="static-content" style="width:100%">
                            </textarea>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type ="submit" id="edit-static" class ="btn btn-default">
                            Submit
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <p class="text-center">©Copyright 2015 Tai, Chet, and Ashley</p>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/securehome.js"></script>

    </body>
</html>
