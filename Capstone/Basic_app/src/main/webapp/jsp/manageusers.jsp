<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dispensation|Drones</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/personalizedcss.css" rel="stylesheet"/>
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/drone.jpg">
    </head>
    <body>
        <div class="container">

            <h1>Dispensation</h1>

            <div class="col-md-2 col-sm-offset-2 row top-buffer-2">
            </div>
            <div class="container">
                <form method="post" action="j_spring_security_check" class="signin form-inline col-md-2 pull-right" role="form">
                    <input type="button" onclick="location.href = '${pageContext.request.contextPath}/j_spring_security_logout'" value="Log Out" />
                </form>
            </div>
            <div class="navbar">
                <ul id="nav">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/addpost">Add Post</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/approve">Approve</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/manageusers">Manage Users</a></li>
                </ul>
            </div>
        </div>
        <div class ="row">
            <div class ="col-md-5 col-md-offset-2">
                <h2></h2>
                <table class="table table-hover">
                    <tbody id="usertable">
                    </tbody>
                </table>
            </div>
            <a href="manageusers.jsp"></a>
       
        </div>


        <!-- Modal Edit user-->
        <div id="manageModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit User</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <p id="modal_id"></p>
                            <!--<p>User Name</p>-->
                            <h4 id="fieldone"></h4>
                            <hr>
                            <p>New Password</p>
                            <input type="password" id="fieldtwo">
                            </input>
                            <br><br>
                            <p>Repeat New Password</p>
                            <input type="password" id="fieldthree">
                            </input>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type ="submit" id="edit-user" class ="btn btn-default">
                            Submit
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal Add user-->
        <div id="addModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add User</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <p id="modal_id"></p>
                            <p>User Name</p>
                            <input type="text" id="add_username">
                            </input>
                            <p id="fieldone"></p>
                            <p>Password</p>
                            <input type="password" id="add_pass">
                            </input>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type ="submit" id="add-user" class ="btn btn-default">
                            Submit
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>


            </div>
        </div>
        <div class="footer-dif">
            <p class="text-center">©Copyright 2015 Tai, Chet, and Ashley</p>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/manageusers.js"></script>

    </body>
</html>