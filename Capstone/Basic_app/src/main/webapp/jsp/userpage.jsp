<%-- 
    Document   : userpage
    Created on : Nov 14, 2015, 4:59:02 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dispensation|Drones</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/personalizedcss.css" rel="stylesheet"/>
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/drone.jpg">
    </head>
    <body>
         <img src="${pageContext.request.contextPath}/img/extreme-drone.jpg" id="background" width="100%" height="100%">
        <div class="container">

            <h1>Dispensation</h1>

            <div class="col-md-2 col-sm-offset-2 row top-buffer-2">
            </div>
            <div class="container">
                <form method="post" action="j_spring_security_check" class="signin form-inline col-md-2 pull-right" role="form">
                    <!--<div class="form-group">-->
                    <!--                        <label for="username">Username:</label>
                                            <input type="text" name="j_username" class="form-control" id="username" >
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password:</label>
                                            <input type="password" name="j_password" class="form-control" id="password" >
                                        </div>
                                        <button type="submit" class="btn btn-default center-block">Submit</button>-->
                    <input type="button" onclick="location.href = '${pageContext.request.contextPath}/j_spring_security_logout'" value="Log Out" />
                </form>

            </div>
            <div class="navbar">
                <ul id="nav">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/userpage">Home</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/useraddpost">Add Post</a></li>
                </ul>
            </div>

            <div class ="row">
                <div class ="col-md-8">
                    <h2></h2>
                    <table class="table table-hover">
                        <tbody id="blogroll">
                        </tbody>
                    </table>

                </div>
                <div id="sidebar">
                    <p class="news"> Dispensation is a privately owned drone manufacturing company based out of Minneapolis Minnesota. We are developing drones that can go anywhere in the world. </p>
                    <h3>Tags</h3>
                    <table>
                        <tr>
                            <th width="50%"></th>
                            <th width="50%"></th>
                        </tr>
                        <tbody id="tagroll">

                        </tbody>
                    </table>
                    <p class="news"> Dispensation <br> 601 Nicollet Mall <br> Minneapolis, MN 55402 </p>
                </div>
            </div>
        </div>
        <div class="footer">
            <p class="text-center">©Copyright 2015 Tai, Chet, and Ashley</p>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/user.js"></script>
    </body>
</html>

