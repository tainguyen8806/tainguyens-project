<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
         <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/drone.jpg">
        <title>Dispensation|Drones</title>
    </head>
    <body>
        <div class="container">
            <h1>Dispensation</h1> 
            <div>
                <h1>An error has occurred</h1>
                <h3>${errorMessage}</h3>
                <h3>DINO MAD</h3>

                <div class="col-md-2">
                    <a href="${pageContext.request.contextPath}/img/dino.jpg">
                        <img src="${pageContext.request.contextPath}/img/dino.jpg" style="width:200px;height:200p"x>
                    </a>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    </body>
</html>