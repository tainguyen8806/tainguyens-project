<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dispensation|Drones</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
         <link href="${pageContext.request.contextPath}/css/personalizedcss.css" rel="stylesheet"/>
        <!-- Dis Icon -->
         <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/drone.jpg">
    </head>
    <body>
        <div class="container">
            <div class="col-sm-1" >
                <h1>Dispensation</h1>
            </div>

            <div class="container">
                <form method="post" action="j_spring_security_check" class="signin form-inline col-md-2 pull-right" role="form">
                    <input type="button" onclick="location.href = '${pageContext.request.contextPath}/j_spring_security_logout'" value="Log Out" />
                </form>
            </div>
            <div class="navbar">
                <ul id="nav">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/securehome">Home</a></li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/addpost">Add Post</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/approve">Approve</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/manageusers">Manage User</a></li>
                </ul>
            </div>
            <div class ="row">
                <div class ="col-md-6">
                    <h2>Add New Post</h2>
                    <form>
                        <h4>Title</h4>
                        <input type="text" id="title" style="width: 50%" />
                        
                        <textarea id="blog-content" style="width:100%">
                        </textarea>
                        <br>
                        Tags<input type="text" id="tag"/><br><br>
                        Date<input type="date" id="date"/><br><br>
                        Expiration Date<input type="date" id="expired-date" /><br><br>
                        <button type ="submit" id="add-post" class ="btn btn-default">
                            Submit
                        </button>
                    </form>
                    <div id="validationErrors" style="color: red"/>
                    <br><br>
                </div>
            </div>

            <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>

            <script src="${pageContext.request.contextPath}/js/post.js"></script>

            <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
            <script type="text/javascript">
                        tinymce.init({
                            selector: "textarea#blog-content",
                            plugins: [
                                "advlist autolink lists link image charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        });
            </script>

    </body>
</html>
