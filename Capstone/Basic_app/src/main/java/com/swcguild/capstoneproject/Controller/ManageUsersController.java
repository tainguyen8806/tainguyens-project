/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.capstoneproject.Controller;

import com.swcguild.capstoneproject.dao.PostListDao;
import com.swcguild.capstoneproject.model.User;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class ManageUsersController {

    private PostListDao dao;

    @Inject
    public ManageUsersController(PostListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/alluser", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers() {
        return dao.getAllUsers();
    }

    @RequestMapping(value = "/getuser/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@PathVariable("id") int id) {
        return dao.getUserById(id);
    }

    @RequestMapping(value = "/alluser/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateUserById(@PathVariable("id") int id, @RequestBody User user) {
        dao.updateUser(user);
    }

    @RequestMapping(value = "/alluser/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("id") int id) {
        dao.removeUser(id);
    }

    @RequestMapping(value = "/alluser", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public User createUser(@RequestBody User user) {
        dao.addUser(user);
        return user;
    }
}
