/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.capstoneproject.Controller;

import com.swcguild.capstoneproject.dao.PostListDao;
import com.swcguild.capstoneproject.model.StaticContent;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class StaticController {

    private PostListDao dao;

    @Inject
    public StaticController(PostListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/staticcontent", method = RequestMethod.GET)
    @ResponseBody
    public StaticContent getContent() {
        return dao.getContent();

    }

    @RequestMapping(value = "/staticcontent", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatic(@RequestBody StaticContent sc) {
        dao.updateStatic(sc);
    }

}
