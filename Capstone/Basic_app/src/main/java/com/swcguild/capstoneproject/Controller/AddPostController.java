/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.capstoneproject.Controller;

import com.swcguild.capstoneproject.dao.PostListDao;
import com.swcguild.capstoneproject.model.Post;
import com.swcguild.capstoneproject.model.PostTag;
import com.swcguild.capstoneproject.model.Tag;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class AddPostController {

    private PostListDao dao;

    @Inject
    public AddPostController(PostListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/addpost", method = RequestMethod.GET)
    public String displayAddPostPage() {
        return ("addpost");
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Post createPost(@Valid @RequestBody Post post) {
        dao.addPost(post);
        return post;
    }

    @RequestMapping(value = "/allpost", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getAllPost() {
// get all of the Post from the data layer
        return dao.getAllPost();

    }

    @RequestMapping(value = "/tag", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Tag createTag(@Valid @RequestBody Tag tag) {
        dao.addTag(tag);
        return tag;
    }

    @RequestMapping(value = "postTag", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public PostTag createdPostTag(@Valid @RequestBody PostTag pt) {
        dao.addPostTag(pt);
        return pt;
    }
}
