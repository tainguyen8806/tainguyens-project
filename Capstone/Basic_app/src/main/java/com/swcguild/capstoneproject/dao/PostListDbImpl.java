/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.capstoneproject.dao;

import com.swcguild.capstoneproject.model.Post;
import com.swcguild.capstoneproject.model.PostTag;
import com.swcguild.capstoneproject.model.StaticContent;
import com.swcguild.capstoneproject.model.Tag;
import com.swcguild.capstoneproject.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class PostListDbImpl implements PostListDao {

    private final static String SQL_INSERT_POST
            = "insert into post (content,title,date,expired_date,status) values(?,?,?,?,?)";

    private final static String SQL_INSERT_TAG
            = "insert into tag (tag_name) values (?)";

    private final static String SQL_INSERT_POST_TAG
            = "insert into post_tag (post_id, tag_id) values (?,?)";

    private final static String SQL_UPDATE_POST
            = "update post set title = ?, content = ?, date = ? , expired_date = ?, status = ? where post_id = ?";

    private final static String SQL_UPDATE_POST_BY_ID
            = "update post set title = ?, content = ? where post_id = ?";

    private final static String SQL_SELECT_ALL_POSTS
            = "select * from post where status='a' and curdate()>= date and curdate()<= expired_date order by date desc";

    private final static String SQL_SELECT_POST_BY_ID
            = "select * from post where post_id=?";

    private final static String SQL_SELECT_APPROVE_POSTS
            = "select * from post where status='p'";

    private final static String SQL_SELECT_ALL_TAGS
            = "select * from tag group by tag_name";

    private final static String SQL_SELECT_POST_BY_TAG
            = "select * from post as p join post_tag as pt on p.post_id = pt.post_id "
            + "join tag as t on pt.tag_id = t.tag_id where t.tag_name=(select tag_name from tag where tag_id=? order by date desc)";

    private final static String SQL_SELECT_STATIC_CONTENT
            = "select * from static_content where content_id = 1";

    private final static String SQL_UPDATE_STATIC
            = "update static_content set content = ? where content_id = 1";

    private final static String SQL_SELECT_ALL_USERS
            = "select * from users u join authorities a on u.username = a.username where a.authority = 'ROLE_USER'";

    private final static String SQL_SELECT_USER_BY_ID
            = "select * from users where user_id = ?";

    private final static String SQL_UPDATE_USER
            = "update users set password = ? where user_id = ?";

    private final static String SQL_DELETE_USER
            = "DELETE FROM `Blog`.`users` WHERE `users`.`user_id` = ?";

    private final static String SQL_DELETE_USER_ROLE
            = "delete from authorities where username = ?";
    
    private final static String SQL_ADD_USER
            = "insert into users (username, password, enabled) values (?, ?, ?)";
    
    private final static String SQL_ADD_USER_ROLES
            = "insert into authorities (username, authority) values (?, ?)";

    private JdbcTemplate jdbcTemplate;

    public void setjdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static final class PostMapper implements ParameterizedRowMapper<Post> {

        @Override
        public Post mapRow(ResultSet rs, int i) throws SQLException {
            Post post = new Post();
            post.setPostId(rs.getInt("post_id"));
            post.setBlogContent(rs.getString("content"));
            post.setTitle(rs.getString("title"));
            post.setDate(rs.getString("date"));
            post.setExpiredDate(rs.getString("expired_date"));
            post.setStatus(rs.getString("status"));

            return post;
        }
    }

    private static final class TagMapper implements ParameterizedRowMapper<Tag> {

        @Override
        public Tag mapRow(ResultSet rs, int i) throws SQLException {
            Tag tag = new Tag();
            tag.setTagId(rs.getInt("tag_id"));
            tag.setTagName(rs.getString("tag_name"));
            return tag;
        }
    }

    private static final class SCMapper implements ParameterizedRowMapper<StaticContent> {

        @Override
        public StaticContent mapRow(ResultSet rs, int i) throws SQLException {
            StaticContent sc = new StaticContent();
            sc.setId(rs.getInt("content_id"));
            sc.setContent(rs.getString("content"));
            return sc;
        }
    }

    private static final class UserMapper implements ParameterizedRowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setUserid(rs.getInt("user_id"));
            user.setName(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            return user;
        }
    }

    @Override
    public StaticContent getContent() {
        StaticContent sc = jdbcTemplate.queryForObject(SQL_SELECT_STATIC_CONTENT, new SCMapper());
        return sc;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateStatic(StaticContent sc) {
        jdbcTemplate.update(SQL_UPDATE_STATIC,
                sc.getContent());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updatePostById(Post post) {
        jdbcTemplate.update(SQL_UPDATE_POST_BY_ID,
                post.getTitle(),
                post.getBlogContent(),
                post.getPostId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Post addPost(Post post) {
        jdbcTemplate.update(SQL_INSERT_POST,
                post.getBlogContent(),
                post.getTitle(),
                post.getDate(),
                post.getExpiredDate(),
                post.getStatus());
        post.setPostId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return post;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Tag addTag(Tag tag) {
        jdbcTemplate.update(SQL_INSERT_TAG,
                tag.getTagName());
        tag.setTagId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class
        ));

        return tag;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public PostTag addPostTag(PostTag pt) {
        jdbcTemplate.update(SQL_INSERT_POST_TAG,
                pt.getPostId(),
                pt.getTagId());
        return pt;
    }

    @Override
    public List<Post> getAllPost() {
        return jdbcTemplate.query(SQL_SELECT_ALL_POSTS, new PostMapper());

    }

    @Override
    public List<Tag> getAllTag() {
        return jdbcTemplate.query(SQL_SELECT_ALL_TAGS, new TagMapper());
    }

    @Override
    public List<Post> getApprovePost() {
        return jdbcTemplate.query(SQL_SELECT_APPROVE_POSTS, new PostMapper());
    }

    @Override
    public List<Post> getPostByTag(int tagId) {
        return jdbcTemplate.query(SQL_SELECT_POST_BY_TAG, new PostMapper(), tagId);
    }

    @Override
    public Post getPostById(int postId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_POST_BY_ID, new PostMapper(), postId);
    }

    @Override
    public void updatePost(Post post) {
        jdbcTemplate.update(SQL_UPDATE_POST,
                post.getTitle(),
                post.getBlogContent(),
                post.getDate(),
                post.getExpiredDate(),
                post.getStatus(),
                post.getPostId());
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SQL_SELECT_ALL_USERS, new UserMapper());
    }

    @Override
    public User getUserById(int id) {
        return jdbcTemplate.queryForObject(SQL_SELECT_USER_BY_ID, new UserMapper(), id);
    }

    @Override
    public void updateUser(User user) {
        jdbcTemplate.update(SQL_UPDATE_USER,
                user.getPassword(),
                user.getUserid());

    }

    @Override
    public void removeUser(int id) {
        User user = getUserById(id);
        jdbcTemplate.update(SQL_DELETE_USER_ROLE, user.getName());
        jdbcTemplate.update(SQL_DELETE_USER, id);
    }

    @Override
    public User addUser(User user) {
        jdbcTemplate.update(SQL_ADD_USER, user.getName(),
                user.getPassword(),
                1);
        user.setUserid(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        jdbcTemplate.update(SQL_ADD_USER_ROLES, user.getName(),
                "ROLE_USER");
        return user;
    }

}
