package com.swcguild.capstoneproject.Controller;

import com.swcguild.capstoneproject.dao.PostListDao;
import com.swcguild.capstoneproject.model.Post;
import com.swcguild.capstoneproject.model.Tag;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class HomeController {

    private PostListDao dao;

    @Inject
    public HomeController(PostListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return ("home");
    }

    @RequestMapping(value = "/userpage", method = RequestMethod.GET)
    public String displayUserPage() {
        return ("userpage");
    }

    @RequestMapping(value = "/useraddpost", method = RequestMethod.GET)
    public String displayUserAddPost() {
        return ("useraddpost");
    }

    @RequestMapping(value = {"/securehome"}, method = RequestMethod.GET)
    public String displaySecureHomePage() {
        return ("securehome");
    }

    @RequestMapping(value = {"/approve"}, method = RequestMethod.GET)
    public String displayApprovePage() {
        return ("approve");
    }

    @RequestMapping(value = {"/manageusers"}, method = RequestMethod.GET)
    public String displayManageUsersPage() {
        return ("manageusers");
    }

//    @RequestMapping(value = "/posts", method = RequestMethod.GET)
//    @ResponseBody
//    public List<Post> getAllPosts() {
//        return dao.getAllPost();
//
//    }
    @RequestMapping(value = "/alltags", method = RequestMethod.GET)
    @ResponseBody
    public List<Tag> getAllTag() {
        return dao.getAllTag();
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getPostByTag(@PathVariable("id") int id) {
        return dao.getPostByTag(id);
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePostById(@PathVariable("id") int id, @RequestBody Post post) {
// set the value of the PathVariable id on the incoming Contact object
// to ensure that a) the contact id is set on the object and b) that
// the value of the PathVariable id and the Contact object id are the // same.
        post.setPostId(id);
// update the contact
        dao.updatePostById(post);
    }

    @RequestMapping(value = "/getpost/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Post getPostById(@PathVariable("id") int id) {
        return dao.getPostById(id);
    }
    
    @RequestMapping(value = "/deletepost/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePostById(@PathVariable ("id") int id, @RequestBody Post post){
        dao.updatePost(post);
    }
}
