/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.capstoneproject.Controller;

import com.swcguild.capstoneproject.dao.PostListDao;
import com.swcguild.capstoneproject.model.Post;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */

@Controller
public class ApproveController {

    private PostListDao dao;

    @Inject
    public ApproveController(PostListDao dao) {
        this.dao = dao;
    }
    
//    @RequestMapping(value = "/approve",method = RequestMethod.GET)
//    public String displayApprovePage(){
//        return ("approve");
//    }
    
    
    @RequestMapping(value = "/approveposts", method = RequestMethod.GET)
    @ResponseBody
    
    public List<Post> getApprovePost(){
        return dao.getApprovePost();
    }
    
    @RequestMapping(value = "/approvepost/{id}", method = RequestMethod.GET)
    @ResponseBody
    
    public Post getPostById(@PathVariable("id") int postId){
        return dao.getPostById(postId);
    }
    
    @RequestMapping(value = "/approvepost/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePost(@PathVariable("id") int id, @Valid @RequestBody Post post){
        post.setPostId(id);
        dao.updatePost(post);
    }
    

}
