/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.capstoneproject.dao;

import com.swcguild.capstoneproject.model.Post;
import com.swcguild.capstoneproject.model.PostTag;
import com.swcguild.capstoneproject.model.StaticContent;
import com.swcguild.capstoneproject.model.Tag;
import com.swcguild.capstoneproject.model.User;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface PostListDao {
    
    public void updateStatic(StaticContent sc);
    public Post addPost(Post post);
    public Post getPostById(int postId);
    public void updatePost(Post updatepost);
    public List<Post> getAllPost();
    public List<Post> getApprovePost();
    public List<Tag> getAllTag();
    public List<Post> getPostByTag(int tagId);
    public Tag addTag(Tag tag);
    public PostTag addPostTag(PostTag pt);
    public StaticContent getContent();
    public void updatePostById(Post post2);
    public List<User> getAllUsers();
    public User getUserById(int id);
    public void updateUser (User user);
    public void removeUser (int id);
    public User addUser(User user);
    
}
