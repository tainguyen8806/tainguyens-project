//package com.swcguild.capstoneproject.dao;
//
//import com.swcguild.capstoneproject.model.Post;
//import com.swcguild.capstoneproject.model.PostTag;
//import com.swcguild.capstoneproject.model.StaticContent;
//import com.swcguild.capstoneproject.model.Tag;
//import com.swcguild.capstoneproject.model.User;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// *
// * @author apprentice
// */
//public class PostListDaoTest {
//
//    private PostListDao dao;
//
//    public PostListDaoTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
//        dao = ctx.getBean("postListDao", PostListDao.class);
//
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("delete from post");
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of updateStatic method, of class PostListDao.
//     */
//    @Test
//    public void testUpdateStatic() {
//        System.out.println("updateStatic");
//        StaticContent sc = null;
//        PostListDao instance = new PostListDaoImpl();
//        instance.updateStatic(sc);
//
//    }
//
//    /**
//     * Test of addPost method, of class PostListDao.
//     */
//    @Test
//    public void testAddPost() {
//
//        Post post = new Post();
//        post.setBlogContent("In tree");
//        post.setTitle("Cat");
//        post.setDate("2015-12-10");
//        post.setExpiredDate("2016-01-14");
//        post.setStatus("a");
//        post.setPostId(56);
//        dao.addPost(post);
//
//        Post expResult = dao.getPostById(post.getPostId());
//
//        assertEquals(expResult, post);
//
//    }
//
//    /**
//     * Test of getPostById method, of class PostListDao.
//     */
//    @Test
//    public void testGetPostById() {
//
//        Post post = new Post();
//        post.setBlogContent("Fun times");
//        post.setTitle("Driving");
//        post.setDate("2015-11-10");
//        post.setExpiredDate("2016-12-14");
//        post.setStatus("a");
//        post.setPostId(5);
//        dao.addPost(post);
//
//        Post expResult = dao.getPostById(post.getPostId());
//
//        assertEquals(expResult, post);
//
//    }
//
//    /**
//     * Test of updatePost method, of class PostListDao.
//     */
//    @Test
//    public void testUpdatePost() {
//
//        Post post = new Post();
//        post.setBlogContent("In tree");
//        post.setTitle("Cat");
//        post.setDate("2015-12-10");
//        post.setExpiredDate("2015-01-10");
//        post.setStatus("a");
//        dao.addPost(post);
//        post.setBlogContent("There are many.");
//        dao.updatePost(post);
//        Post expResult = dao.getPostById(post.getPostId());
//
//        assertEquals(expResult, post);
//    }
//
//    /**
//     * Test of getAllPost method, of class PostListDao.
//     */
//    @Test
//    public void testGetAllPost() {
//
//        Post test = new Post();
//        test.setBlogContent("Buy a new drone");
//        test.setTitle("Buy now");
//        test.setDate("2015-12-01");
//        test.setExpiredDate("2015-01-01");
//        test.setStatus("a");
//        dao.addPost(test);
//
//        Post now = new Post();
//        now.setBlogContent("Come to black friday!");
//        now.setTitle("SALE");
//        now.setDate("2015-11-15");
//        now.setExpiredDate("2015-12-15");
//        now.setStatus("a");
//        dao.addPost(now);
//
//        List<Post> pList = dao.getAllPost();
//        assertEquals(pList.size(), 1);
//
//    }
//
//    /**
//     * Test of getApprovePost method, of class PostListDao.
//     */
//    @Test
//    public void testGetApprovePost() {
//        Post test = new Post();
//        test.setBlogContent("Buy a new drone");
//        test.setTitle("Buy now");
//        test.setDate("2015-12-01");
//        test.setExpiredDate("2015-01-01");
//        test.setStatus("a");
//        dao.addPost(test);
//
//        Post now = new Post();
//        now.setBlogContent("Come to black friday!");
//        now.setTitle("SALE");
//        now.setDate("2015-11-15");
//        now.setExpiredDate("2015-12-15");
//        now.setStatus("a");
//        dao.addPost(now);
//
//        Post nowone = new Post();
//        nowone.setBlogContent("Come to black friday!");
//        nowone.setTitle("SALE");
//        nowone.setDate("2015-11-15");
//        nowone.setExpiredDate("2015-12-15");
//        nowone.setStatus("p");
//        dao.addPost(nowone);
//
//        Post wone = new Post();
//        wone.setBlogContent("Come to black friday!");
//        wone.setTitle("SALE");
//        wone.setDate("2015-11-15");
//        wone.setExpiredDate("2015-12-15");
//        wone.setStatus("p");
//        dao.addPost(wone);
//
//        List<Post> pList = dao.getAllPost();
//        assertEquals(pList.size(), 1);
//    }
//
//    /**
//     * Test of getAllTag method, of class PostListDao.
//     */
//    @Test
//    public void testGetAllTag() {
//
//        Tag tag = new Tag();
//        tag.setTagId(4);
//        tag.setTagName("Fun");
//        dao.addTag(tag);
//
//        Tag tags = new Tag();
//        tags.setTagId(4);
//        tags.setTagName("Fun");
//        dao.addTag(tags);
//
//        List<Tag> tList = dao.getAllTag();
//        assertEquals(tList.size(), 7);
//    }
//
//    /**
//     * Test of getPostByTag method, of class PostListDao.
//     */
//    @Test
//    public void testGetPostByTag() {
//        Post post = new Post();
//        post.setBlogContent("Welcome");
//        post.setTitle("Now!");
//        post.setDate("2015-11-13");
//        post.setExpiredDate("2015-12-13");
//        post.setStatus("a");
//        post.setPostId(10);
//
//        dao.addPost(post);
//
//        Post result = dao.getPostById(post.getPostId());
//
//        assertEquals(result, post);
//    }
//
//    /**
//     * Test of addTag method, of class PostListDao.
//     */
//    @Test
//    public void testAddTag() {
//        System.out.println("addTag");
//        Tag tag = null;
//        PostListDao instance = new PostListDaoImpl();
//        Tag expResult = null;
//        Tag result = instance.addTag(tag);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//
//
//    }
//
//    /**
//     * Test of addPostTag method, of class PostListDao.
//     */
//    @Test
//    public void testAddPostTag() {
//        System.out.println("addPostTag");
//        PostTag pt = null;
//        PostListDao instance = new PostListDaoImpl();
//        PostTag expResult = null;
//        PostTag result = instance.addPostTag(pt);
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of getContent method, of class PostListDao.
//     */
//    @Test
//    public void testGetContent() {
//        System.out.println("getContent");
//        PostListDao instance = new PostListDaoImpl();
//        StaticContent expResult = null;
//        StaticContent result = instance.getContent();
//        assertEquals(expResult, result);
// 
//    }
//
//    /**
//     * Test of updatePostById method, of class PostListDao.
//     */
//    @Test
//    public void testUpdatePostById() {
//       
//        
//        System.out.println("update");
//        PostListDao instance = new PostListDaoImpl();
//        List<User> expResult = null;
//        List<User> result = instance.getAllUsers();
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of getAllUsers method, of class PostListDao.
//     */
//    @Test
//    public void testGetAllUsers() {
//        System.out.println("getAllUsers");
//        PostListDao instance = new PostListDaoImpl();
//        List<User> expResult = null;
//        List<User> result = instance.getAllUsers();
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of getUserById method, of class PostListDao.
//     */
//    @Test
//    public void testGetUserById() {
//        System.out.println("getUserById");
//        int id = 0;
//        PostListDao instance = new PostListDaoImpl();
//        User expResult = null;
//        User result = instance.getUserById(id);
//        assertEquals(expResult, result);
//   
//    }
//
//    public class PostListDaoImpl implements PostListDao {
//
//        public void updateStatic(StaticContent sc) {
//        }
//
//        public Post addPost(Post post) {
//            return null;
//        }
//
//        public Post getPostById(int postId) {
//            return null;
//        }
//
//        public void updatePost(Post updatepost) {
//        }
//
//        public List<Post> getAllPost() {
//            return null;
//        }
//
//        public List<Post> getApprovePost() {
//            return null;
//        }
//
//        public List<Tag> getAllTag() {
//            return null;
//        }
//
//        public List<Post> getPostByTag(int tagId) {
//            return null;
//        }
//
//        public Tag addTag(Tag tag) {
//            return null;
//        }
//
//        public PostTag addPostTag(PostTag pt) {
//            return null;
//        }
//
//        public StaticContent getContent() {
//            return null;
//        }
//
//        public void updatePostById(Post post2) {
//        }
//
//        public List<User> getAllUsers() {
//            return null;
//        }
//
//        public User getUserById(int id) {
//            return null;
//        }
//
//        @Override
//        public void updateUser(User user) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public void removeUser(int id) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        @Override
//        public User addUser(User user) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//    }
//
//}
