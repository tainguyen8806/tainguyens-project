/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.library.dao;

import com.swcguild.library.model.Author;
import com.swcguild.library.model.HBook;
import com.swcguild.library.model.Publisher;
import java.util.List;

public interface HLibraryDao {

    public void addAuthor(Author author);

    public void deleteAuthor(Author author);

    public void updateAuthor(Author author);

    public Author getAuthorById(int id);
// Just get the book - the author objects will be there...
//public List<Author> getAuthorsByBookId(int bookId);

    public List<Author> getAllAuthors();

    public void addBook(HBook book);

    public void deleteBook(HBook book);

    public void updateBook(HBook book);

    public HBook getBookById(int id);
//public List<Book> getBooksByAuthorId(int authorId);
//public List<Book> getBooksByPublisherId(int publisherId);

    public List<HBook> getAllBooks();

    public void addPublisher(Publisher publisher);

    public void deletePublisher(Publisher publisher);

    public void updatePublisher(Publisher publisher);

    public Publisher getPublisherById(int id);
//public Publisher getPublisherByBookId(int bookId);

    public List<Publisher> getAllPublishers();
}
